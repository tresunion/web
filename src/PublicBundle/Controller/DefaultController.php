<?php

namespace PublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Eleve;
use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\EleveType;
use ApiBundle\Form\Type\InscriptionEleveType;
use ApiBundle\Entity\InscriptionEleve;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PublicBundle:Default:index.html.twig');
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addEleveAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $eleve = new Eleve();
        $form = $this->createForm(EleveType::class, $eleve, array(
            'validation_groups' => array('create','Default'),
        ));
        
        $form->submit($request->request->all());
        
        if ($form->isValid()) {
            $inscription = new InscriptionEleve();
            $inscrForm = $this->createForm(InscriptionEleveType::class, $inscription, array(
                'validation_groups' => array('create','Default'),
            ));
            $inscrForm->submit($request->request->all());
            if($inscrForm->isValid()) {
                $eleve->setStatus(Eleve::INSCRIT);
                $event = $this->get('api.event')
                    ->createEventEleve(Event::CATEGORIE_PRE_INSCRIPTION_MANUEL, $eleve, $this->getUser());
                $em->persist($event);
                $em->persist($inscrForm);
                $em->persist($eleve);
                $em->flush();
                return $eleve;
            }
        }
        return $form;
    }
}
        