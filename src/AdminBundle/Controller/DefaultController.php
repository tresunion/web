<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserBundle\Entity\User;



class DefaultController extends Controller
{
    public function indexAction()
    {
      return $this->render('AdminBundle:Default:layout.html.twig');
    }

    public function searchAction(Request $req){
      $em = $this->container->get('doctrine')->getEntityManager();
      if($req->isXMLHttpRequest()){
        $value=$req->get('value_send');
        $eleveRepository = $this->getDoctrine()->getRepository('ApiBundle:Eleve');
        $intervenantRepository = $this->getDoctrine()->getRepository('ApiBundle:Intervenant');
        $evenementRepository = $this->getDoctrine()->getRepository('ApiBundle:Evenement');
        $evenements=$evenementRepository->findAllBy($value);
        $eleves=$eleveRepository->findAllBy($value);
        $intervenants=$intervenantRepository->findAllBy($value);
        return new JsonResponse(array("eleves" => $eleves, "intervenants" => $intervenants, "evenements" => $evenements));
      }
      return new Response("fail",400);
    }
}
