<?php

namespace UserBundle\Security;

use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class MyAccessDeniedHandler implements AccessDeniedHandlerInterface
{

    public function handle(Request $request, AccessDeniedException $accessDeniedException){

	    if ($request->isXmlHttpRequest()) {
	        $response = new Response(json_encode(array('status' => 'protected')));
	        return $response;
	    }
	    else {
	        return new RedirectResponse($this->router->generate('login'));
	    }
	}
}