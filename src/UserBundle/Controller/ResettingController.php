<?php
// src/AppBundle/Controller/RegistrationController.php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ResettingController as BaseController;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;


class ResettingController extends BaseController
{
  public function checkEmailAction(Request $request)
    {
      $username = $request->query->get('username');
      if (empty($username)) {
        // the user does not come from the sendEmail action
        return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
      }

      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserByEmail($username);

      if(!$user){
        return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
      }

      if($user->hasRole('ROLE_ADMIN')) 
        return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));

      // return $this->redirect($this->generateUrl('admin_homepage'));

      return $this->render('FOSUserBundle:Resetting:check_email.html.twig', array(
          'tokenLifetime' => ceil($this->container->getParameter('fos_user.resetting.token_ttl') / 3600),
      ));
    }

    /**
     * Reset user password.
     *
     * @param Request $request
     * @param string  $token
     *
     * @return Response
     */
    public function resetAction(Request $request, $token)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.resetting.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        $form = $formFactory->createForm();
        $form->setData($user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);
            $userManager->updateUser($user);
            if (null === $response = $event->getResponse()) {
              $session = $request->getSession();

              // add flash messages
              $session->getFlashBag()->add(
                'success',
                'Votre mot de passe a été reintialisé avec succès'
              );
                // return $this->redirect($this->generateUrl('fos_user_security_login'));
                // $url = $this->generateUrl('fos_user_profile_show');
                $url =  $this->generateUrl('fos_user_security_login');
                $response = new RedirectResponse($url);
            }
            $dispatcher->dispatch(
                FOSUserEvents::RESETTING_RESET_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );
            return $response;
        }
        return $this->render('FOSUserBundle:Resetting:reset.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }
}

?>
