<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\NiveauScolaireRepository")
 * @ORM\Table(name="niveauScolaire")
 */
class NiveauScolaire
{
    public function __construct() {
        
        $this->listeInscriptions = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"public"})
     */
    protected $id;

    public function getId(){
      return $this->id;
    }
    
    /**
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"public"})
     */
    protected $nom;
    
    public function getNom(){
        return $this->nom;
    }
    
    public function setNom($nom){
        $this->nom=$nom;
    }
    
}

?>
