<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\EventRepository")
 * @ORM\Table(name="event")
 */
class Event
{

    // Pour $type
    const TYPE_ELEVE = 0;
    const TYPE_INTERVENANT = 1;
    const TYPE_SEANCE = 2;

    // Pour eleve
    const CATEGORIE_CREATION = 0;
    const CATEGORIE_MODIFICATION = 1;
    const CATEGORIE_SUPPRESSION = 2;
    const CATEGORIE_PRE_INSCRIPTION_MANUEL = 3;
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId(){
      return $this->id;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;
    
    public function getType(){
        return $this->type;
    }
    
    public function setType($type){
        $this->type=$type;
    }
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $categorie;
    
    public function getCategorie(){
        return $this->categorie;
    }
    
    public function setCategorie($categorie){
        $this->categorie=$categorie;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="listeEvents")
     */
    protected $auteur;
    
    public function getAuteur(){
        return $this->auteur;
    }
    
    public function setAuteur($auteur){
        $this->auteur=$auteur;
    }
    
    /**
     * @ORM\Column(type="string")
     */
    protected $data;
    
    public function getData(){
        return $this->data;
    }
    
    public function setData($data){
        $this->data=$data;
    }

    protected $text;

    public function getText(){
        return $this->text;
    }

    public function setText($text){
        $this->text = $text;
    }

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    public function getCreated(){
      return $this->created;
    }

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function getUpdated(){
      return $this->updated;
    }

    public function toString()
    {
        return $this->getAuteur()+"; "+$this.getCategorie()+"; "+$this.getData();
    }
}

?>
