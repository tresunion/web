<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\IntervenantRepository")
 * @ORM\Table(name="intervenant")
 */
class Intervenant
{
    
    public function __construct() {
        
        $this->listeSeances = new ArrayCollection();
        $this->listeInscriptions = new ArrayCollection();
        
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId(){
      return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nom;

    public function getNom(){
      return $this->nom;
    }

    public function setNom($nom){
      $this->nom=$nom;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $prenom;

    public function getPrenom(){
      return $this->prenom;
    }

    public function setPrenom($prenom){
      $this->prenom=$prenom;
    }

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $sexe;

    public function getSexe(){
      return $this->sexe;
    }

    public function setSexe($sexe){
      $this->sexe=$sexe;
    }

    /**
     * @ORM\Column(type="datetime", length=15, nullable=true)
     */
    protected $dateNaissance;

    public function getDateNaissance(){
      return $this->dateNaissance;
    }

    public function setDateNaissance($dateNaissance){
      $this->dateNaissance=$dateNaissance;
    }

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $telephone;

    public function getTelephone(){
      return $this->telephone;
    }

    public function setTelephone($telephone){
      $this->telephone=$telephone;
    }

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $mail;

    public function getMail(){
      return $this->mail;
    }

    public function setMail($mail){
      $this->mail=$mail;
    }
    
    /**
     * @ORM\ManyToMany(targetEntity="ApiBundle\Entity\Seance", cascade={"persist"}, inversedBy="listeIntervenants")
     */
    protected $listeSeances;
    
    public function getListeSeances(){
        return $this->listeSeances;
    }
    
    public function addSeance(Seance $seance){
        return $this->listeSeances[] = $seance;
        return this;
    }
    
    public function removeSeance(Seance $seance) {
        $this->listeSeances->removeElement($seance);
    }
    
    public function setListeSeances($listeSeances){
        $this->listeSeances = $listeSeances;
    }
    
    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\InscriptionIntervenant", mappedBy="intervenant")
     */
    protected $listeInscriptions;
    
    public function getListeInscriptions(){
        return $this->listeInscriptions;
    }
    
    public function addInscription(InscriptionIntervenant $inscriptionIntervenant){
        return $this->listeInscriptions[] = $inscriptionIntervenant;
        return this;
    }
    
    public function removeInscription(InscriptionEleve $inscriptionIntervenant) {
        $this->listeInscriptions->InscriptionIntervenant($inscriptionIntervenant);
    }
    
    public function setListeInscriptions($listeInscriptions){
        $this->listeInscriptions = $listeInscriptions;
    }

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    public function getCreated(){
      return $this->created;
    }

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function getUpdated(){
      return $this->updated;
    }

    public function toString()
    {
        return $this->getNom()+" "+$this.getPrenom();
    }
}

?>
