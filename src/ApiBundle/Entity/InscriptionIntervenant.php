<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\InscriptionIntervenantRepository")
 * @ORM\Table(name="inscriptionIntervenant")
 */
class InscriptionIntervenant
{
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId(){
      return $this->id;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Intervenant", inversedBy="listeInscriptions")
     */
    protected $intervenant;
    
    public function getIntervenant(){
        return $this->intervenant;
    }
    
    public function setIntervenant($intervenant){
        $this->intervenant=$intervenant;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status=$status;
    }

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    public function getCreated(){
      return $this->created;
    }

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function getUpdated(){
      return $this->updated;
    }

    public function toString()
    {
        return $this->getNom()+" "+$this.getPrenom();
    }
}

?>
