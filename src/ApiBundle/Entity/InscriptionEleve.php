<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity()
 * @ORM\Table(name="inscriptionEleve")
 */
class InscriptionEleve
{

    const PRE_INSCRIT = 0;
    const INSCRIT = 1;
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId(){
      return $this->id;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Eleve", inversedBy="listeInscriptions")
     */
    protected $eleve;
    
    public function getEleve(){
        return $this->eleve;
    }
    
    public function setEleve($eleve){
        $this->eleve=$eleve;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\NiveauScolaire")
     */
    protected $niveauScolaire;

    public function getNiveauScolaire(){
      return $this->niveauScolaire;
    }

    public function setNiveauScolaire($niveauScolaire){
        $this->niveauScolaire=$niveauScolaire;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Etablissement")
     */
    protected $etablissement;

    public function getEtablissement(){
      return $this->etablissement;
    }

    public function setEtablissement($etablissement){
        $this->etablissement=$etablissement;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status=$status;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Evenement", inversedBy="inscriptionEleve")
     */
    protected $evenement;
    
    public function getEvenement(){
        return $this->evenement;
    }
    
    public function setEvenement($evenement){
        $this->evenement=$evenement;
    }

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    public function getCreated(){
      return $this->created;
    }

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function getUpdated(){
      return $this->updated;
    }

    public function toString()
    {
        return $this->getNom()+" "+$this.getPrenom();
    }
}

?>
