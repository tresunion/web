<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

// Pour $status;

define("PLANNED", 0);   //Plannifi�
define("DONE", 1);      // Termin�
define("VALIDATED", 2); // Valid�

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\SeanceRepository")
 * @ORM\Table(name="seance")
 * @Assert\Callback(methods={"isDateValid"})
 */
class Seance
{

    // Comme la propriété $categories doit être un ArrayCollection,
    
    // On doit la définir dans un constructeur :
    
    public function __construct() {
        
        $this->listeEleves = new ArrayCollection();
        $this->listeIntervenants = new ArrayCollection();
    }

    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    public function getId(){
        return $this->id;
    }
    
    /*
     * TODO:
     * OK une date début,
     * OK une date fin,
     * OK un statut (pour linstant je vois un statut planifié, un statut terminé, un statut validé),
     * une liste d'élèves
     * liste d'intervenants
     */
    
    /**
     * @var \DateTime $dateStart
     *
     * @ORM\Column(type="datetime")
     */
    protected $dateStart;
    
    public function getDateStart(){
        return $this->dateStart;
    }
    
    public function setDateStart($dateStart){
        $this->dateStart = dateStart;
    }
    
    /**
     * @var \DateTime $dateEnd
     *
     * @ORM\Column(type="datetime")
     */
    protected $dateEnd;
    
    public function getDateEnd(){
        return $this->$dateEnd;
    }
    
    public function setDateEnd($dateEnd){
        $this->dateEnd = $dateEnd;
    }
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $status;
    
    /*
     * 0 = Planned/Plannifié
     * 1 = Done/Terminé
     * 2 = Validated/Validé
     */
    public function getStatus(){
        return $this->status;
    }
    
    public function setStatus($status){
        $this->status = $status;
    }
    
    /**
     * @ORM\ManyToMany(targetEntity="ApiBundle\Entity\Eleve", cascade={"persist"}, mappedBy="listeSeances")
     */
    protected $listeEleves;
    
    public function getListeEleves(){
        return $this->listeEleves;
    }
    
    public function addEleve(Eleve $eleve){
        return $this->listeEleves[] = $eleve;
        return this;
    }
    
    public function removeEleve(Eleve $eleve) {
        $this->listeEleves->removeElement($eleve);
    }

    public function setListeEleves($listeEleves){
        $this->listeEleves = $listeEleves;
    }

    /**
     * @ORM\ManyToMany(targetEntity="ApiBundle\Entity\Intervenant", cascade={"persist"}, mappedBy="listeIntervenants")
     */
    protected $listeIntervenants;
    
    public function getListeIntervenants(){
        return $this->listeIntervenants;
    }
    
    public function addIntervenant(Intervenant $intervenant){
        return $this->listeIntervenants[] = $intervenant;
        return this;
    }
    
    public function removeIntervenant(Intervenant $intervenant) {
        $this->listeIntervenants->removeElement($intervenant);
    }

    public function setListeIntervenants($listeIntervenants){
        $this->listeIntervenants = $listeIntervenants;
    }
    
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    public function getCreated(){
        return $this->created;
    }
    
    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;
    
    public function getUpdated(){
        return $this->updated;
    }
    
    public function toString()
    {
        return $this->getDateStart()+" "+$this.getDateEnd();
    }
    
    public function isDateValid(ExecutionContext $context)
    {
        if ($this->startDate->getTimestamp() > $this->endDate->getTimestamp()) {
            $propertyPath = $context->getPropertyPath() . '.startDate';
            $context->setPropertyPath($propertyPath);
            $context->addViolation('The starting date must be anterior than the ending date !', array(), null);
        }
        if ($this->endDate->getTimestamp() > new \DateTime()) {
            $propertyPath = $context->getPropertyPath() . '.endDate';
            $context->setPropertyPath($propertyPath);
            $context->addViolation('The ending date must be anterior than the current date !', array(), null);
        }
    }
}

?>
