<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;


/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\EvenementRepository")
 * @ORM\Table(name="evenement")
 */
class Evenement
{

    public function __construct() {
        $this->listeEvenements = new ArrayCollection();
        $this->inscriptionsEleve = new ArrayCollection();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"public"})
     */
    protected $id;

    public function getId(){
      return $this->id;
    }

    /**
     * @ORM\Column(type="string")
     * @JMS\Groups({"public"})
     */
    protected $libelle;
    
    public function getLibelle(){
        return $this->libelle;
    }
    
    public function setLibelle($libelle){
        $this->libelle=$libelle;
    }
    
    
    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Evenement", mappedBy="evenementLinked")
     */
    protected $listeEvenements;
    
    public function getListeEvenements(){
        return $this->listeEvenements;
    }
    
    public function addEvenement(Evenement $evenement){
        return $this->listeEvenements[] = $evenement;
    }
    
    public function removeEvenement(Evenement $evenement) {
        $this->listeEvenements->removeElement($evenement);
    }
    
    public function setListeEvenements($listeEvenements){
        $this->listeEvenements = $listeEvenements;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Evenement", inversedBy="listeEvenements")
     */
    protected $evenementLinked;
    
    public function getEvenementLinked(){
        return $this->evenementLinked;
    }
    
    public function setEvenementLinked($evenementLinked){
        $this->evenementLinked = $evenementLinked;
    }
    
    public function isPrincipal() {
        return $this->evenementLinked !== NULL;
    }
    
    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\InscriptionEleve", mappedBy="evenement")
     */
    protected $inscriptionsEleve;
    
    public function getInscriptionsEleve(){
        return $this->inscriptionsEleve;
    }

    public function addInscriptionEleve(InscriptionEleve $inscriptionEleve){
        $this->inscriptionsEleve[] = $inscriptionEleve;
    }
    
    public function removeInscriptionEleve(InscriptionEleve $inscriptionEleve) {
        $this->inscriptionsEleve->removeElement($inscriptionEleve);
    }
    
    public function setInscriptionsEleve($inscriptionsEleve){
        $this->inscriptionsEleve = $inscriptionsEleve;
    }
    
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    public function getCreated(){
      return $this->created;
    }

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    public function getUpdated(){
      return $this->updated;
    }

    public function toString()
    {
        return $this->getAuteur()+"; "+$this.getCategorie()+"; "+$this.getData();
    }

    /**
     * @var \DateTime $dateDebut
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"public"})
     */
    protected $dateDebut;
    
    public function getDateDebut(){
        return $this->dateDebut;
    }
    
    public function setDateDebut($dateDebut){
        $this->dateDebut = $dateDebut;
    }
    
    /**
     * @var \DateTime $dateFin
     *
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"public"})
     */
    protected $dateFin;
    
    public function getDateFin(){
        return $this->dateFin;
    }
    
    public function setDateFin($dateFin){
        $this->dateFin = $dateFin;
    }

    /**
     * @ORM\Column(name="actif", type="boolean", nullable=true)
     */
    private $actif;

    public function getActif(){
        return $this->actif;
    }
    
    public function setActif($actif){
        $this->actif = $actif;
    }
    
    /**
     * @Assert\IsTrue(message="Insertion ok")
     */
    public function isDateValid()
    {
        if ($this->dateDebut->getTimestamp() > $this->dateFin->getTimestamp()) {
            return false;
        }
        $now = new \DateTime();
        if ($this->dateFin->getTimestamp() <  $now->getTimestamp()) {
            return false;
        }
        return true;
    }
}

?>
