<?php

namespace ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SeanceRepository extends EntityRepository
{
    public function findAllByStartDate($value)
    {
        return $this
            ->createQueryBuilder('s')
            ->where('s.$dateStart = :value')
            ->setParameter('value', $value)
            ->getQuery()
            ->getArrayResult();
    }
    
    public function findAllByEndDate($value)
    {
        return $this
        ->createQueryBuilder('s')
        ->where('s.$dateEnd = :value')
        ->setParameter('value', $value)
        ->getQuery()
        ->getArrayResult();
    }
    
    public function findAllByStartAndEndDate($dateStart, $dateEnd)
    {
        return $this
        ->createQueryBuilder('s')
        ->where('s.$dateEnd = :dateEnd AND s.$dateStart = :dateStart')
        ->setParameter('dateStart', $dateStart)
        ->setParameter('dateEnd', $dateEnd)
        ->getQuery()
        ->getArrayResult();
    }
    
}

?>
