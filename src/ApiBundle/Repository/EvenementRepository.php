<?php

namespace ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class EvenementRepository extends EntityRepository
{

    public function findAllBy($value)
    {
        return $this
            ->createQueryBuilder('p')
            ->where('p.libelle LIKE :value')
            ->setParameter('value','%'.$value.'%')
            ->getQuery()
            ->getArrayResult();
    }

    public function findAllFilterPaginator($count, $page, $sorting, $filter){
    	$qb = $this->createQueryBuilder('p');
	    $qb->setFirstResult(($page-1) * $count)
	       ->setMaxResults($count);
 
 		if($sorting){
 			$champ = array_keys($sorting)[0];
 			$typeSort = $sorting[$champ];

 			$qb->orderBy('p.'.$champ, $typeSort);
 		}
 		else{
 			$qb->orderBy('p.dateFin', 'desc');
 		}

 		if($filter){
 			foreach($filter as $champ => $filtre){
 				$qb->andWhere('p.'.$champ." LIKE '%".$filtre."%'");
 			}
 		}

    	return new Paginator($qb);
    }
    
}

?>
