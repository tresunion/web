<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Evenement;
use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\EvenementType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class EvenementsController extends Controller
{

	/**
     * @Rest\View()
     */
    public function getEvenementsAction(Request $request)
    {
        $count = $request->get('count');
        $page = $request->get('page');
        $sorting = $request->get('sorting');
        $filter = $request->get('filter');

        if(!$count){
            $count = 20;
        }

        if(!$page){
            $page = 1;
        }

        $evenements = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
                ->findAllFilterPaginator($count, $page, $sorting, $filter);

        return array("evenements" => iterator_to_array($evenements->getIterator()), "total" => count($evenements));
    }
    
    /**
     * @Rest\View()
     */
    public function getEvenementAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }
        
        return $evenement;
    }

    /**
     * @Rest\View()
     * @Rest\Delete()
     */
    public function removeEvenementAction($evenementId, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $evenement = $em->getRepository('ApiBundle:Evenement')
                ->find($evenementId);

        if(!$this->get('api.evenement')->estSupprimable($evenement)){
            return new JsonResponse(['message' => "Cet évènement ne peut être supprimé"], Response::HTTP_BAD_REQUEST);
        }

        if($evenement){
            $em->remove($evenement);
            $em->flush();
        }

        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addEvenementAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $evenement = new Evenement();
        $form = $this->createForm(EvenementType::class, $evenement, array(
            'validation_groups' => array('create','Default'),
        ));
        
        $form->submit($request->request->all());
        
        if ($form->isValid()) {
            $em->persist($evenement);
            $em->flush();
            return $evenement;
        }
        
        return $form;
    }
    
    /**
     * @Rest\View()
     * @Rest\Put()
     */
    public function updateEvenementAction($evenementId, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $evenement = $em->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if (!$evenement) {
            return new JsonResponse(['message' => 'Evenement non trouvé'], Response::HTTP_NOT_FOUND);
        }
        
        $form = $this->createForm(EvenementType::class, $evenement, array(
            'validation_groups' => array('Default'),
        ));
        
        $form->submit($request->request->all(), false);
        
        if ($form->isValid()) {
            $em->merge($evenement);
            $em->flush();
            return $evenement;
        }
        return $form;
    }

}
