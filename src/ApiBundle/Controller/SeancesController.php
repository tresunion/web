<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Seance;
use ApiBundle\Form\Type\SeanceType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

class SeancesController extends Controller
{
    /**
     * @Rest\View()
     */
    public function getSeancesAction()
    {
        $seances = $this->getDoctrine()->getRepository('ApiBundle:Seance')
        ->findAll();
        
        // Gestion de la réponse
        return $seances;
    }
    
    /**
     * @Rest\View()
     */
    public function getSeanceAction($seanceId, Request $request)
    {
        $seance = $this->getDoctrine()->getRepository('ApiBundle:Seance')
        ->find($seanceId);
        
        if(!$seance){
            return new JsonResponse(['message' => 'Seance not found'], Response::HTTP_NOT_FOUND);
        }
        
        return $seance;
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addSeanceAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $seance = new Seance();
        $form = $this->createForm(SeanceType::class, $seance);
        
        $form->submit($request->request->all());
        
        if ($form->isValid()) {
            $em->persist($seance);
            $em->flush();
            return $seance;
        } else {
            return $form;
        }
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete()
     */
    public function removeSeanceAction($seanceId, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $seance = $em->getRepository('ApiBundle:Seance')
        ->find($seanceId);
        
        if($seance && $seance->getStatus() != 2){
            $em->remove($seance);
            $em->flush();
        }
    }
    
    /**
     * @Rest\View()
     * @Rest\Put()
     */
    public function updateSeanceAction($seanceId, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        
        $seance = $em->getRepository('ApiBundle:Seance')
        ->find($seanceId);
        
        if (!$seance) {
            return new JsonResponse(['message' => 'Séance non trouvée'], Response::HTTP_NOT_FOUND);
        }
        
        $form = $this->createForm(SeanceType::class, $seance);
        
        $form->submit($request->request->all(), false);
        
        if ($form->isValid()) {
            $em->merge($seance);
            $em->flush();
            return $seance;
        } else {
            return $form;
        }
    }
    
    
}