<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Etablissement;
use ApiBundle\Form\Type\EtablissementType;
use ApiBundle\Entity\NiveauScolaire;
use ApiBundle\Form\Type\NiveauScolaireType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class DonneesController extends Controller
{

	/**
     * @Rest\View()
     */
    public function getDonneesAction($type, Request $request)
    {

        $count = $request->get('count');
        $page = $request->get('page');
        $sorting = $request->get('sorting');
        $filter = $request->get('filter');

        if(!$count){
            $count = 20;
        }

        if(!$page){
            $page = 1;
        }

        if($type == 1){
            $donnees = $this->getDoctrine()->getRepository('ApiBundle:Etablissement')
                    ->findAllFilterPaginator($count, $page, $sorting, $filter);
        }
        else if($type == 2){
            $donnees = $this->getDoctrine()->getRepository('ApiBundle:NiveauScolaire')
                    ->findAllFilterPaginator($count, $page, $sorting, $filter);
        }
        else{
            return new JsonResponse(['message' => "Type de donnée inconnu"], Response::HTTP_NOT_FOUND);
        }
        return array("donnees" => iterator_to_array($donnees->getIterator()), "total" => count($donnees));
    }

    /**
     * @Rest\View()
     * @Rest\Get("/donnees/{type}/{donneeId}")
     */
    public function getDonneeAction($type, $donneeId, Request $request)
    {
        if($type == 1){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Etablissement')
                    ->find($donneeId);
        }
        else if($type == 2){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:NiveauScolaire')
                    ->find($donneeId);
        }

        if(!$donnee){
        	return new JsonResponse(['message' => "La donnée n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        return $donnee;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addDonneeAction($type, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

        if($type == 1){
            $donnee = new Etablissement();
            $form = $this->createForm(EtablissementType::class, $donnee);
        }
        else if($type == 2){
            $donnee = new NiveauScolaire();
            $form = $this->createForm(NiveauScolaireType::class, $donnee);
        }
        else{
            return new JsonResponse(['message' => "Ce type de donnée n'existe pas"], Response::HTTP_NOT_FOUND);
        }

    	$form->submit($request->request->all());

    	if ($form->isValid()) {
            $em->persist($donnee);
            $em->flush();
            return $donnee;
        } else {
 			return $form;
        }
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/donnees/{type}/multipleAdd")
     */
    public function addDonneesAction($type, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        $donneesEnvoyees = $request->request->all();

        $forms = [];

        if($type == 1){
            foreach($donneesEnvoyees as $donneeEnvoyee){
                $donnee = new Etablissement();
                $form = $this->createForm(EtablissementType::class, $donnee);
                $form->submit($donneeEnvoyee);
                $forms[] = $form;
            }
        }
        else if($type == 2){
            foreach($donneesEnvoyees as $donneeEnvoyee){
                $donnee = new NiveauScolaire();
                $form = $this->createForm(NiveauScolaireType::class, $donnee);
                $form->submit($donneeEnvoyee);
                $forms[] = $form;
            }
        }
        else{
            return new JsonResponse(['message' => "Ce type de donnée n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        foreach($forms as $form){
            if ($form->isValid()) {
                $em->persist($form->getData());
            } 
            else {
                return $form;
            }
        }

        $em->flush();
        return $donneesEnvoyees;

    }

    /**
     * @Rest\View()
     * @Rest\Delete("/donnees/{type}/{donneeId}/remove")
     */
    public function removeDonneeAction($type, $donneeId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

        if($type == 1){
        	$donnee = $em->getRepository('ApiBundle:Etablissement')
                    ->find($donneeId);
        }
        else if($type == 2){
            $donnee = $em->getRepository('ApiBundle:NiveauScolaire')
                    ->find($donneeId);
        }
        else{
            return new JsonResponse(['message' => "Ce type de donnée n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        if($donnee){
	        $em->remove($donnee);
	        $em->flush();
        }

        if(!$donnee){
            return new JsonResponse(['message' => "La donnée n'éxiste pas"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put("/donnees/{type}/{donneeId}/update")
     */
    public function updateDonneeAction($type, $donneeId, Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        if($type == 1){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Etablissement')
                    ->find($donneeId);
            $form = $this->createForm(EtablissementType::class, $donnee, array(
                'validation_groups' => array('Default'),
            ));
        }
        else if($type == 2){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:NiveauScolaire')
                    ->find($donneeId);
            $form = $this->createForm(NiveauScolaireType::class, $donnee, array(
                'validation_groups' => array('Default'),
            ));
        }

        if (!$donnee) {
            return new JsonResponse(['message' => 'Donnée non trouvée'], Response::HTTP_NOT_FOUND);
        }

        $form->submit($request->request->all(), false);

        if ($form->isValid()) {
            // $event = $this->get('api.event')
            // ->createEventEleve(Event::CATEGORIE_MODIFICATION, $donnee, $this->getUser());
            // $em->persist($event);
            $em->merge($donnee);
            $em->flush();
            return $donnee;
        } else {
            return $form;
        }
    }


}
