<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\EventType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class EventsController extends Controller
{

	/**
     * @Rest\View()
     */
    public function getEventsAction(Request $request)
    {
        $count = $request->get('count');
        $page = $request->get('page');
        $sorting = $request->get('sorting');
        $filter = $request->get('filter');

        if(!$count){
            $count = 20;
        }

        if(!$page){
            $page = 1;
        }

        $events = $this->getDoctrine()->getRepository('ApiBundle:Event')
                ->findAllFilterPaginator($count, $page, $sorting, $filter);
        
        foreach($events as $event){
            $event->setText($this->get('api.event')->getText($event));
            
        }
        
        return array("events" => iterator_to_array($events->getIterator()), "total" => count($events));
    }

    /**
     * @Rest\View()
     */
    public function getEventAction($eventId, Request $request)
    {
        $event = $this->getDoctrine()->getRepository('ApiBundle:Event')
        ->find($eventId);

        if(!$event){
        	return new JsonResponse(['message' => "L'evènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        return $event;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addEventAction(Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

    	$event = new Event();
    	$form = $this->createForm(EventType::class, $event);

    	$form->submit($request->request->all());

    	if ($form->isValid()) {
    	    $em->persist($event);
            $em->flush();
            return $event;
        } else {
 			return $form;
        }
    }

    /**
     * @Rest\View()
     * @Rest\Delete()
     */
    public function removeEventAction($eventId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();
    	$event = $em->getRepository('ApiBundle:Event')
    	->find($eventId);

    	if($event){
    	    $em->remove($event);
	        $em->flush();
        }

        if(!$event){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put()
     */
    public function updateEventAction($eventId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

    	$event = $em->getRepository('ApiBundle:Event')
    	->find($eventId);

    	if (!$event) {
            return new JsonResponse(['message' => 'évènement non trouvé'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(EventType::class, $event);

    	$form->submit($request->request->all(), false);

    	if ($form->isValid()) {
    	    $em->merge($event);
            $em->flush();
            return $event;
        } else {
            return $form;
        }
    }

}
