<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Eleve;
use ApiBundle\Entity\Event;
use ApiBundle\Entity\InscriptionEleve;
use ApiBundle\Form\Type\EleveType;
use ApiBundle\Form\Type\InscriptionEleveType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class PublicController extends Controller
{

	/**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addElevePublicAction(Request $request)
    {
        $em = $this->container->get('doctrine')->getEntityManager();

        $eleveRequest = $request->request->all();
        $inscriptionRequest = $eleveRequest['inscription'];
        unset($eleveRequest['inscription']);

        $eleve = new Eleve();
        $form = $this->createForm(EleveType::class, $eleve, array(
            'validation_groups' => array('Default'),
        ));

        $form->submit($eleveRequest);
        if ($form->isValid()) {
            $eleveDejaExistant = $this->getDoctrine()->getRepository('ApiBundle:Eleve')
                    ->findOneBy(array("nom" => $eleve->getNom(), "prenom" => $eleve->getPrenom(), "dateNaissance" => $eleve->getDateNaissance(), "sexe" => $eleve->getSexe()));
            if($eleveDejaExistant){
                $eleve = $eleveDejaExistant;
            }
            $inscriptionEleve = new InscriptionEleve();
            $formInscription = $this->createForm(InscriptionEleveType::class, $inscriptionEleve, array(
                'validation_groups' => array('create','Default'),
            ));
            $formInscription->submit($inscriptionRequest);
            if($formInscription->isValid()){
                $inscriptionEleveExistant = $this->getDoctrine()->getRepository('ApiBundle:InscriptionEleve')
                    ->findOneBy(array("eleve" => $eleve, "evenement" => $inscriptionEleve->getEvenement()));
                if($inscriptionEleveExistant){
                    return new JsonResponse(['message' => "Cet enfant est déjà inscrit à cet évènement"], Response::HTTP_NOT_FOUND);
                }
                $inscriptionEleve->setStatus(InscriptionEleve::PRE_INSCRIT);
                $inscriptionEleve->setEleve($eleve);
                $em->persist($eleve);
                $em->persist($inscriptionEleve);
                $em->flush();
                return $eleve;
            }
            else{
                return $formInscription;
            }
        }
        else{
            return $form;
        }
    }

    /**
     * @Rest\View(serializerGroups={"public"})
     */
    public function getEtablissementsPublicAction(Request $request)
    {
        $etablissements = $this->getDoctrine()->getRepository('ApiBundle:Etablissement')
                    ->findAll();
        
        return array("etablissements" => $etablissements);
    }

    /**
     * @Rest\View(serializerGroups={"public"})
     */
    public function getNiveauxScolairesPublicAction(Request $request)
    {
        $niveauxScolaires = $this->getDoctrine()->getRepository('ApiBundle:NiveauScolaire')
                    ->findAll();
        
        return array("niveauxScolaires" => $niveauxScolaires);
    }


    /**
     * @Rest\View(serializerGroups={"public"})
     */
    public function getEvenementsPublicAction(Request $request)
    {
        $evenements = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
                ->findAll(array(), array('dateDebut' => 'DESC'));

        return array("evenements" => $evenements);
    }

    /**
     * @Rest\View(serializerGroups={"public"})
     */
    public function getEvenementPublicAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
                ->find($evenementId);

        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        return $evenement;
    }

}
