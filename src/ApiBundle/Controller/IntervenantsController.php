<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Intervenant;
use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\IntervenantType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

class IntervenantsController extends Controller
{

	/**
     * @Rest\View()
     */
    public function getIntervenantsAction(Request $request)
    {
        $count = $request->get('count');
        $page = $request->get('page');
        $sorting = $request->get('sorting');
        $filter = $request->get('filter');

        if(!$count){
            $count = 20;
        }

        if(!$page){
            $page = 1;
        }

        $intervenants = $this->getDoctrine()->getRepository('ApiBundle:Intervenant')
                ->findAllFilterPaginator($count, $page, $sorting, $filter);

        return array("intervenants" => iterator_to_array($intervenants->getIterator()), "total" => count($intervenants));
    }

    /**
     * @Rest\View()
     */
    public function getIntervenantAction($intervenantId, Request $request)
    {
        $intervenant = $this->getDoctrine()->getRepository('ApiBundle:Intervenant')
                ->find($intervenantId);

        if(!$intervenant){
        	return new JsonResponse(['message' => "L'intervenant n'éxiste pas"], Response::HTTP_NOT_FOUND);
        }

        return $intervenant;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addIntervenantAction(Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

    	$intervenant = new Intervenant();
    	$form = $this->createForm(IntervenantType::class, $intervenant, array(
            'validation_groups' => array('create','Default'),
        ));

    	$form->submit($request->request->all());

    	if ($form->isValid()) {
    	    $event = $this->get('api.event')
    	       ->createEventIntervenant(Event::CATEGORIE_CREATION, $intervenant, $this->getUser());
            $em->persist($event);
            $em->persist($intervenant);
            $em->flush();
            return $intervenant;
        } else {
 			return $form;
        }
    }

    /**
     * @Rest\View()
     * @Rest\Delete()
     */
    public function removeIntervenantAction($intervenantId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();
        $intervenant = $em->getRepository('ApiBundle:Intervenant')
                ->find($intervenantId);

        if($intervenant){
            $event = $this->get('api.event')
            ->createEventIntervenant(Event::CATEGORIE_SUPPRESSION, $intervenant, $this->getUser());
            $em->persist($event);
	        $em->remove($intervenant);
	        $em->flush();
        }

        if(!$intervenant){
            return new JsonResponse(['message' => "L'intervenant n'éxiste pas"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put()
     */
    public function updateIntervenantAction($intervenantId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

        $intervenant = $em->getRepository('ApiBundle:Intervenant')
                ->find($intervenantId);

        if (!$intervenant) {
            return new JsonResponse(['message' => 'Intervenant non trouvé'], Response::HTTP_NOT_FOUND);
        }

    	$form = $this->createForm(IntervenantType::class, $intervenant, array(
            'validation_groups' => array('Default'),
        ));

    	$form->submit($request->request->all(), false);

    	if ($form->isValid()) {
    	    $event = $this->get('api.event')
    	    ->createEventIntervenant(Event::CATEGORIE_MODIFICATION, $intervenant, $this->getUser());
    	    $em->persist($event);
            $em->merge($intervenant);
            $em->flush();
            return $intervenant;
        } else {
            return $form;
        }
    }

}
