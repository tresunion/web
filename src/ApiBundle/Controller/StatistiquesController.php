<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\EventType;
use ApiBundle\Entity\Etablissement;
use ApiBundle\Form\Type\EtablissementType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use ApiBundle\Entity\InscriptionEleve;

class StatistiquesController extends Controller
{

	/**
     * @Rest\View()
     */
    public function repartitionHommeFemmeAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        $inscriptionsEleves = $evenement->getInscriptionsEleve();
        $nbHomme = 0;
        $nbFemme = 0;
        foreach($inscriptionsEleves as $inscriptionEleve){
            if($inscriptionEleve->getEleve()->getSexe() === "homme"){
                $nbHomme++;
            }
            else if($inscriptionEleve->getEleve()->getSexe() === "femme"){
                $nbFemme++;
            }
        }

        $data = [];
        $data[] = [ 'key' => 'Homme', 'y' => $nbHomme ];
        $data[] = [ 'key' => 'Femme', 'y' => $nbFemme ];

        return array("data" => $data);
    }

    /**
     * @Rest\View()
     */
    public function repartitionAgeEvenementAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        $inscriptionsEleves = $evenement->getInscriptionsEleve();
        $data = [];
        foreach($inscriptionsEleves as $inscriptionEleve){
            $dateNaissance = $inscriptionEleve->getEleve()->getDateNaissance();
            $age = date("Y") - date("Y", strtotime($dateNaissance->format('Y-m-d')));
            if(!array_key_exists($age, $data)){
                $data[$age] = 0;
            }
            $data[$age]++;
        }

        $values = [];
        foreach($data as $age => $value){
            $valuee['label'] = $age;
            $valuee['value'] = $value;
            $values[] = $valuee;
        }

        $valueFinal['key'] = "test";
        $valueFinal['values'] = $values;

        return array("data" => array($valueFinal));
    }

    /**
     * @Rest\View()
     * @Rest\Get("/statistiques/voir")
     */
    public function getStatistiquesAction(Request $request)
    {

        $stats = [];

        $donneesEleves = $this->getDoctrine()->getRepository('ApiBundle:Eleve')
            ->findAll();
        $stats["eleves"] = $this->get('api.statistiques')->getElevesStats($donneesEleves);

        $donneesIntervenants = $this->getDoctrine()->getRepository('ApiBundle:Intervenant')
            ->findAll();
        $stats["intervenants"] = $this->get('api.statistiques')->getIntervenantsStats($donneesIntervenants);

        $donneesEtablissements = $this->getDoctrine()->getRepository('ApiBundle:Etablissement')
            ->findAll();
        $stats["etablissements"] = $this->get('api.statistiques')->getEtablissementsStats($donneesEtablissements);

        $donneesEvenements = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
            ->findAll();
        $stats["evenements"] = $this->get('api.statistiques')->getEvenementsStats($donneesEvenements);

        $donneesSeances = $this->getDoctrine()->getRepository('ApiBundle:Seance')
            ->findAll();
        $stats["seances"] = $this->get('api.statistiques')->getSeancesStats($donneesSeances);

        return $stats;
    }

    /**
     * @Rest\View()
     */
    public function repartitionEtablissementEvenementAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        $inscriptionsEleves = $evenement->getInscriptionsEleve();
        $data = [];
        foreach($inscriptionsEleves as $inscriptionEleve){
            $etablissementNom = $inscriptionEleve->getEtablissement()->getNom();
            if(!array_key_exists($etablissementNom, $data)){
                $data[$etablissementNom] = 0;
            }
            $data[$etablissementNom]++;
        }

        $dataFinal = [];
        foreach($data as $key => $value){
            $dataFinall['key'] = $key;
            $dataFinall['y'] = $value;
            $dataFinal[] = $dataFinall;
        }

        return array("data" => $dataFinal);
    }

    /**
     * @Rest\View()
     */
    public function repartitionNiveauScolaireEvenementAction($evenementId, Request $request)
    {
        $evenement = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
        ->find($evenementId);
        
        if(!$evenement){
            return new JsonResponse(['message' => "L'évènement n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        $inscriptionsEleves = $evenement->getInscriptionsEleve();
        $data = [];
        foreach($inscriptionsEleves as $inscriptionEleve){
            $niveauScolaire = $inscriptionEleve->getNiveauScolaire()->getNom();
            if(!array_key_exists($niveauScolaire, $data)){
                $data[$niveauScolaire] = 0;
            }
            $data[$niveauScolaire]++;
        }

        $dataFinal = [];
        foreach($data as $key => $value){
            $dataFinall['key'] = $key;
            $dataFinall['y'] = $value;
            $dataFinal[] = $dataFinall;
        }

        return array("data" => $dataFinal);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/statistiques/voir/{type}/{donneeId}")
     */
    public function getStatistiqueAction($type, $donneeId, Request $request)
    {
        if($type == 'eleve'){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Eleve')
                    ->find($donneeId);
        }

        elseif($type == 'intervenant'){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Intervenant')
                ->find($donneeId);
        }

        elseif($type == 'evenement'){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Evenement')
                ->find($donneeId);
        }

        elseif($type == 'seance'){
            $donnee = $this->getDoctrine()->getRepository('ApiBundle:Seance')
                ->find($donneeId);
        }

        if(!$donnee){
        	return new JsonResponse(['message' => "La donnée n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        return $donnee;
    }

}
