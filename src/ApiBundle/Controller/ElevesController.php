<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use ApiBundle\Entity\Eleve;
use ApiBundle\Entity\Event;
use ApiBundle\Form\Type\EleveType;
use ApiBundle\Form\Type\InscriptionEleveType;

use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use ApiBundle\Entity\InscriptionEleve;

class ElevesController extends Controller
{

	/**
     * @Rest\View()
     */
    public function getElevesAction(Request $request)
    {
        $count = $request->get('count');
        $page = $request->get('page');
        $sorting = $request->get('sorting');
        $filter = $request->get('filter');

        if(!$count){
            $count = 20;
        }

        if(!$page){
            $page = 1;
        }

        $eleves = $this->getDoctrine()->getRepository('ApiBundle:Eleve')
                ->findAllFilterPaginator($count, $page, $sorting, $filter);

        return array("eleves" => iterator_to_array($eleves->getIterator()), "total" => count($eleves));
    }

    /**
     * @Rest\View()
     */
    public function getEleveAction($eleveId, Request $request)
    {
        $eleve = $this->getDoctrine()->getRepository('ApiBundle:Eleve')
                ->find($eleveId);

        if(!$eleve){
        	return new JsonResponse(['message' => "L'élève n'existe pas"], Response::HTTP_NOT_FOUND);
        }

        return $eleve;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post()
     */
    public function addEleveAction(Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

    	$eleve = new Eleve();
    	$form = $this->createForm(EleveType::class, $eleve, array(
            'validation_groups' => array('create','Default'),
        ));

    	$form->submit($request->request->all());

    	if ($form->isValid()) {
    	    $event = $this->get('api.event')
    	       ->createEventEleve(Event::CATEGORIE_CREATION, $eleve, $this->getUser());
	        $em->persist($event);
	        $em->persist($eleve);
            $em->flush();
            return $eleve;
        }
        
        return $form;
    }

    /**
     * @Rest\View()
     * @Rest\Delete()
     */
    public function removeEleveAction($eleveId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();
    	$eleve = $em->getRepository('ApiBundle:Eleve')
                ->find($eleveId);

        if($eleve){
            $event = $this->get('api.event')
            ->createEventEleve(Event::CATEGORIE_SUPPRESSION, $eleve, $this->getUser());
            $em->persist($event);
	        $em->remove($eleve);
	        $em->flush();
        }

        if(!$eleve){
            return new JsonResponse(['message' => "L'élève n'éxiste pas"], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Put()
     */
    public function updateEleveAction($eleveId, Request $request)
    {
    	$em = $this->container->get('doctrine')->getEntityManager();

    	$eleve = $em->getRepository('ApiBundle:Eleve')
                ->find($eleveId);

        if (!$eleve) {
            return new JsonResponse(['message' => 'Eleve non trouvé'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(EleveType::class, $eleve, array(
            'validation_groups' => array('Default'),
        ));

        $form->submit($request->request->all(), false);

        if ($form->isValid()) {
            $event = $this->get('api.event')
            ->createEventEleve(Event::CATEGORIE_MODIFICATION, $eleve, $this->getUser());
            $em->persist($event);
            $em->merge($eleve);
            $em->flush();
            return $eleve;
        } else {
            return $form;
        }
    }

}
