<?php
namespace ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class IntervenantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom');
        $builder->add('prenom');
        $builder->add('sexe', ChoiceType::class, array(
            'choices'  => array(
                'Homme' => 'homme',
                'Femmme' => 'femme',
            )
        ));
        $builder->add('dateNaissance', DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
        ));
        $builder->add('telephone');
        $builder->add('mail');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ApiBundle\Entity\Intervenant',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

}