<?php
namespace ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class EleveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom');
        $builder->add('prenom');
        $builder->add('sexe', ChoiceType::class, array(
            'choices'  => array(
                'Homme' => 'homme',
                'Femmme' => 'femme',
            )
        ));
        $builder->add('dateNaissance', DateTimeType::class, array(
            'widget' => 'single_text',
        ));
        $builder->add('telephone');
        $builder->add('mail');
        
        $builder->add('listeInscriptions', CollectionType::class, array(
            'entry_type' => InscriptionEleveType::class,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ApiBundle\Entity\Eleve',
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

}