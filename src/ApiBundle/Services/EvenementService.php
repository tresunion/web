<?php

namespace ApiBundle\Services;

use ApiBundle\Entity\Event;

class EvenementService
{
    public function __construct()
    {
    }
    
    // 1 : A venir, 2 : En cours, 3 : Terminé
    public function getStatutEvenement($evenement){
        $now = new \DateTime();
        if($evenement->getDateDebut() > $now){
            return 1;
        }
        else if($evenement->getDateDebut() < $now && $now < $evenement->getDateFin()){
            return 2;
        }
        else if($evenement->getDateFin() < $now){
            return 3;
        }
        return 0;
    }

    public function estSupprimable($evenement){
        if($this->getStatutEvenement($evenement) == 1){
            return true;
        }

        return false;
    }
}
