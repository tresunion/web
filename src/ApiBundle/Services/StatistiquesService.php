<?php

namespace ApiBundle\Services;

use ApiBundle\Entity\Event;

class StatistiquesService
{
    public function __construct()
    {
    }

    public function getElevesStats($donneesEleves) {
        $total = count($donneesEleves);
        $nbParSexes = [];
        $nbNaissancesParAnnees = [];
        $nbTelephones = 0;
        $nbMails = 0;
        $nbSeancesParListes = [];
        $nbInscriptionsParListes = [];

        foreach ($donneesEleves as $eleve) {
            $nbParSexes = $this->ajouteOuInitialise($nbParSexes, $eleve->getSexe());

            if ($eleve->getDateNaissance() != null) {
                $nbNaissancesParAnnees = $this->ajouteOuInitialise($nbNaissancesParAnnees, $eleve->getDateNaissance()->format('Y'));
            }

            if ($eleve->getTelephone() != null) {
                $nbTelephones++;
            }

            if ($eleve->getMail() != null) {
                $nbMails++;
            }

            $nbSeancesParListes = $this->ajouteOuInitialise($nbSeancesParListes, count($eleve->getListeSeances()));

            $nbInscriptionsParListes = $this->ajouteOuInitialise($nbInscriptionsParListes, count($eleve->getListeInscriptions()));
        }

        return [
            "total" => $total,
            "nbParSexes" => $nbParSexes,
            "nbNaissancesParAnnees" => $nbNaissancesParAnnees,
            "nbTelephones" => $nbTelephones,
            "nbMails" => $nbMails,
            "nbSeancesParListes" => $nbSeancesParListes,
            "nbInscriptionsParListes" => $nbInscriptionsParListes
        ];
    }

    public function getIntervenantsStats($donneesIntervenants) {
        $total = count($donneesIntervenants);
        $nbParSexes = [];
        $nbNaissancesParAnnees = [];
        $nbTelephones = 0;
        $nbMails = 0;
        $nbSeancesParListes = [];
        $nbInscriptionsParListes = [];

        foreach ($donneesIntervenants as $intervenant) {
            $nbParSexes = $this->ajouteOuInitialise($nbParSexes, $intervenant->getSexe());

            if ($intervenant->getDateNaissance() != null) {
                $nbNaissancesParAnnees = $this->ajouteOuInitialise($nbNaissancesParAnnees, $intervenant->getDateNaissance()->format('Y'));
            }

            if ($intervenant->getTelephone() != null) {
                $nbTelephones++;
            }

            if ($intervenant->getMail() != null) {
                $nbMails++;
            }

            $nbSeancesParListes = $this->ajouteOuInitialise($nbSeancesParListes, count($intervenant->getListeSeances()));

            $nbInscriptionsParListes = $this->ajouteOuInitialise($nbInscriptionsParListes, count($intervenant->getListeInscriptions()));
        }

        return [
            "total" => $total,
            "nbParSexes" => $nbParSexes,
            "nbNaissancesParAnnees" => $nbNaissancesParAnnees,
            "nbTelephones" => $nbTelephones,
            "nbMails" => $nbMails,
            "nbSeancesParListes" => $nbSeancesParListes,
            "nbInscriptionsParListes" => $nbInscriptionsParListes
        ];
    }

    public function getEtablissementsStats($donneesEtablissements) {
        $total = count($donneesEtablissements);
        $nbParTypes = [];

        foreach ($donneesEtablissements as $etablissement) {
            $nbParTypes = $this->ajouteOuInitialise($nbParTypes, $etablissement->getType());
        }

        return [
            "total" => $total,
            "nbParTypes" => $nbParTypes
        ];
    }

    public function getEvenementsStats($donneesEvenements) {
        $total = count($donneesEvenements);
        $nbDatesDebutParMois = [];
        $nbDatesFinParMois = [];
        $nbParDurees = [];
        $nbParStatuts = [];
        $nbEvenementsParListes = [];
        $nbElevesParInscriptions = [];

        foreach ($donneesEvenements as $evenement) {
            if ($evenement->getDateDebut() != null) {
                $nbDatesDebutParMois = $this->ajouteOuInitialise($nbDatesDebutParMois, $evenement->getDateDebut()->format('M'));
            }

            if ($evenement->getDateFin() != null) {
                $nbDatesFinParMois = $this->ajouteOuInitialise($nbDatesFinParMois, $evenement->getDateFin()->format('M'));
            }

            if ($evenement->getDateDebut() != null && $evenement->getDateFin() != null) {
                $duree = date_diff($evenement->getDateFin(), $evenement->getDateDebut())->format('d');
                $nbParDurees = $this->ajouteOuInitialise($nbParDurees, $duree);
            }

            if ($evenement->getActif() != null) {
                $nbParStatuts = $this->ajouteOuInitialise($nbParStatuts, $evenement->getActif());
            }

            if ($evenement->getListeEvenements() != null) {
                $nbEvenementsParListes = $this->ajouteOuInitialise($nbEvenementsParListes, count($evenement->getListeEvenements()));
            }

            if ($evenement->getInscriptionsEleve() != null) {
                $nbElevesParInscriptions = $this->ajouteOuInitialise($nbElevesParInscriptions, count($evenement->getInscriptionsEleve()));
            }
        }

        return [
            "total" => $total,
            "nbDatesDebutParMois" => $nbDatesDebutParMois,
            "nbDatesFinParMois" => $nbDatesFinParMois,
            "nbParDurees" => $nbParDurees,
            "nbParStatuts" => $nbParStatuts,
            "nbEvenementsParListes" => $nbEvenementsParListes,
            "nbElevesParInscriptions" => $nbElevesParInscriptions
        ];
    }

    public function getSeancesStats($donneesSeances) {
        $total = count($donneesSeances);
        $nbDatesDebutParMois = [];
        $nbDatesFinParMois = [];
        $nbParDurees = [];
        $nbParStatuts = [];
        $nbElevesParlistes = [];
        $nbIntervenantsParListes = [];

        foreach ($donneesSeances as $seance) {
            if ($seance->getDateStart() != null) {
                $nbDatesDebutParMois = $this->ajouteOuInitialise($nbDatesDebutParMois, $seance->getDateStart()->format('m'));
            }

            if ($seance->getDateEnd() != null) {
                $nbDatesFinParMois = $this->ajouteOuInitialise($nbDatesFinParMois, $seance->getDateEnd()->format('m'));
            }

            if ($seance->getDateStart() != null && $seance->getDateEnd() != null) {
                $duree = date_diff($seance->getDateEnd(), $seance->getDateStart())->format('d');
                $nbParDurees = $this->ajouteOuInitialise($nbParDurees, $duree);
            }

            if ($seance->getStatus() != null) {
                $nbParStatuts = $this->ajouteOuInitialise($nbParStatuts, $seance->getStatus());
            }

            if ($seance->getListeEleves() != null) {
                $nbElevesParlistes = $this->ajouteOuInitialise($nbElevesParlistes, count($seance->getListeEleves()));
            }

            if ($seance->getListeIntervenants() != null) {
                $nbIntervenantsParListes = $this->ajouteOuInitialise($nbIntervenantsParListes, count($seance->getListeIntervenants()));
            }
        }

        return [
            "total" => $total,
            "nbDatesDebutParMois" => $nbDatesDebutParMois,
            "nbDatesFinParMois" => $nbDatesFinParMois,
            "nbParDurees" => $nbParDurees,
            "nbParStatuts" => $nbParStatuts,
            "nbElevesParlistes" => $nbElevesParlistes,
            "nbIntervenantsParListes" => $nbIntervenantsParListes
        ];
    }

    private function ajouteOuInitialise($tableau, $valeur) {
        $valeurEnString = strval($valeur);

        if (array_key_exists($valeurEnString, $tableau)) {
            $tableau[$valeurEnString]++;
        }

        else {
            $tableau[$valeurEnString] = 1;
        }

        return $tableau;
    }
}
