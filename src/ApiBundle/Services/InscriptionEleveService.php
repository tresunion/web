<?php

namespace ApiBundle\Services;

use ApiBundle\Entity\InscriptionEleve;

class InscriptionEleveService
{
    public function __construct()
    {
    }
    
    public function createInscriptionEleve($etablissement, $niveau, $eleve) {
        
        $inscriptionEleve = new InscriptionEleve();
        $inscriptionEleve->setEtablissement($etablissement);
        $inscriptionEleve->setNiveauScolaire($niveau);
        $inscriptionEleve->setEleve($eleve);
        $jsonEleve = array (
            "nom"  => $eleve->getNom(),
            "prenom" => $eleve->getPrenom()
        );
        $inscriptionEleve->setData(json_encode($jsonEleve));

        return $inscriptionEleve;
    }

    public function getText($inscriptionEleve){
        $data = json_decode($inscriptionEleve->getData());
        $result .= $inscriptionEleve->getEleve()->getNom()
            ." "
            .$inscriptionEleve->getEleve()->getPrenom();
        
        switch($inscriptionEleve->getStatus()){
            case InscriptionEleve::PRE_REGISTERED:
                $result .= " s'est pre-inscrit ";
                break;
            case InscriptionEleve::REGISTERED:
                $result .= " s'est inscrit ";
                break;
            case InscriptionEleve::ARCHIVED:
                $result .= " a ete archive ";
                break;
            default:
                return " (aucun status...) ";
        }
        
        $result .= " le ".$inscriptionEleve->getCreated();
        
        return $result;
    }
}
?>