<?php

namespace ApiBundle\Services;

use ApiBundle\Entity\Event;

class EventService
{
    public function __construct()
    {
    }
    
    public function createEventEleve($categorie, $eleve, $auteur) {
        
        $event = new Event();
        $event->setType(Event::TYPE_ELEVE);
        $event->setCategorie($categorie);
        $event->setAuteur($auteur);
        $jsonEleve = array (
            "nom"  => $eleve->getNom(),
            "prenom" => $eleve->getPrenom()
        );
        $event->setData(json_encode($jsonEleve));

        return $event;
    }

    public function createEventIntervenant($categorie, $intervenant, $auteur) {

        $event = new Event();
        $event->setType(Event::TYPE_INTERVENANT);
        $event->setCategorie($categorie);
        $event->setAuteur($auteur);
        $jsonIntervenant = array (
            "nom"  => $intervenant->getNom(),
            "prenom" => $intervenant->getPrenom()/*,
            "sexe"  => $intervenant->getSexe(),
            "datenaissance" => $intervenant->getDateNaissance(),
            "telephone" => getTelephone(),
            "mail" => getMail(),
            "seances" => getListeSeances(),
            "creation" => getCreated(),
            "maj" => getUpdated()*/
        );
        $event->setData(json_encode($jsonIntervenant));

        return $event;
    }

    public function getText($event){
        $data = json_decode($event->getData());
        $result = "";
        if($event->getAuteur()) {
            $result .= $event->getAuteur()->getUsername()." ";
        } else {
            $result .= "Une personne anonyme ";
        }
        switch($event->getCategorie()){
            case Event::CATEGORIE_CREATION:
                $result .= "a cree ";
                break;
            case Event::CATEGORIE_MODIFICATION:
                $result .= "a modifie ";
                break;
            case Event::CATEGORIE_SUPPRESSION:
                $result .= "a supprime ";
                break;
            case Event::CATEGORIE_PRE_INSCRIPTION_MANUEL:
                $result .= "a pre-inscrit ";
                break;
            default:
                return "Categorie inconnue";
        }
        switch($event->getType()){
            case Event::TYPE_ELEVE:
                $result .= "l'eleve ".$data->nom." ".$data->prenom;
                break;
            case Event::TYPE_INTERVENANT:
                $result .= "l'intervenant ".$data->nom." ".$data->prenom;
                break;
            default:
                return "Type inconnu";
        }
        return $result;
    }
}
?>