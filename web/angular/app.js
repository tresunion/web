var myApp = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'toastr', 'angular-button-spinner', 'ngTable', 'cp.ngConfirm', 'angularMoment', 'nvd3', 'ui.calendar']);

myApp.run(function($rootScope) {
    $rootScope.breadcrumbTitle="Accueil";
});

angular.module("ngTable").service(
    "$InterpolateUpdateService", function($templateCache, $interpolate){
        'use strict';

        this.changeGridInterpolate = function() {
            var templates = [
                'ng-table/filterRow.html',
                'ng-table/filters/number.html',
                'ng-table/filters/select-multiple.html',
                'ng-table/filters/select.html',
                'ng-table/filters/text.html',
                'ng-table/groupRow.html',
                'ng-table/header.html',
                'ng-table/pager.html',
                'ng-table/sorterRow.html'
            ];

            var start = $interpolate.startSymbol();
            var end = $interpolate.endSymbol();

            for (var i = 0; i < templates.length; i++) {
                var template = templates[i];
                var curTemplate = $templateCache.get(template);
                if (start !== "}}"){
                    curTemplate = curTemplate.replace(/\{\{/g, start);
                }
                if (end !== "}}"){
                    curTemplate = curTemplate.replace(/\}\}/g, end);
                }
                $templateCache.put(template, curTemplate);
            }
        };
    });

angular.module('ngTable').run(function($InterpolateUpdateService) {
    'use strict';

    $InterpolateUpdateService.changeGridInterpolate();
});

myApp.config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
}]);

myApp.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
});

myApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }])
;

myApp.config(function ($httpProvider, $provide) {
    $provide.factory('interceptor', function($q) {
        return {
           'responseError': function(rejection) {
                if (403 === rejection.status) {
                    location.href = "/";
                }
                return $q.reject(rejection);
            }
        };
    });

    $httpProvider.interceptors.push('interceptor');
});

myApp.config(function(toastrConfig) {
    angular.extend(toastrConfig, {
        newestOnTop: true,
        preventDuplicates: false,
        preventOpenDuplicates: false,
        progressBar: true,
        timeOut: 4000,
    });
});