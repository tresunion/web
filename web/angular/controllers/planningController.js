myApp.controller('planningController',['$scope', '$log', '$window', '$rootScope','NgTableParams', 'Api', function($scope, $log, $window, $rootScope, NgTableParams, api){
    $window.document.title = "ADMIN | Planning";
    $rootScope.breadcrumbTitle = "Planning";
    $rootScope.breadcrumb = null;
    $rootScope.breadcrumbActive = "Planning";
    $rootScope.dataLoaded = true;

    $scope.eventSources = [];

    $scope.uiConfig = {
      calendar:{
        height: 800,
        header:{
          left: 'month agendaWeek agendaDay',
          center: 'title',
          right: 'today prev,next'
        },
        lang: 'fr',
        minTime:'08:00:00',
        maxTime:'18:00:00',
        slotDuration:'01:00:00',
        allDaySlot: false
      }
    };

    api.getAllEvenements().then(function(result) {
        evenements = result.data.evenements;
        $scope.eventSources = [];
        $scope.events = [];
        $scope.eventSources.push($scope.events);
        angular.forEach(evenements, function(value, key) {
            evenement = [];
            evenement['title'] = value.libelle;
            evenement['start'] = value.date_debut;
            evenement['end'] = value.date_fin;
            evenement['color'] = '#ea9d9f';
            evenement['url'] = '/admin/evenement/details/'+value.id;
            $scope.events.push(evenement); 
        });

        $rootScope.dataLoaded = true;
    });
    
    $scope.eventSources2 = [
        {
            events: [ // put the array in the `events` property
                {
                    title  : 'event1',
                    start  : '2018-10-01'
                },
                {
                    title  : 'event2',
                    start  : '2018-10-05',
                    end    : '2018-10-07'
                },
            ],
            color: 'black',     // an option!
            textColor: 'yellow' // an option!
        }
    ];

    console.log($scope.eventSources2);

}]);
