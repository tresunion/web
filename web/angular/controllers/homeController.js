myApp.controller('homeController',['$scope', '$log', '$window', '$rootScope','NgTableParams', 'Api', function($scope, $log, $window, $rootScope, NgTableParams, api){
    $window.document.title = "ADMIN | Dashboard";
    $rootScope.breadcrumbTitle = "Accueil";
    $rootScope.breadcrumb = null;
    $rootScope.breadcrumbActive = null;
    $rootScope.dataLoaded = false;
    $scope.eventsTable = new NgTableParams
    (
        {
            page: 1,
            count: 12,
            sorting: { created: "desc" }
        },
        {
            getData: function(params) {
                return api.getAllEvents(params.url()).then(function(result) {
                    $rootScope.dataLoaded = true;
                    params.total(result.data.total);
                    return result.data.events;
                }).catch(function(result) {
	                if(result.status == 404) {
	        			toastr.error(result.data.message);
	                };
                });
            }
        }
    );

}]);
