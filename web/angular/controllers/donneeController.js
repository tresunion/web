myApp.controller('ajouterDonneeController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', function($scope, $log, $window, $rootScope, $routeParams, $api, toastr, $location){
    $scope.donnees = [{}];
    var type = $routeParams.type;
    $scope.type = type;
    if(type == 1){
        $scope.nomDonnee = "Etablissement"
        $rootScope.breadcrumb = "etablissement";
        $scope.titre = "Ajouter un nouvel établissement";
    }
    else if(type == 2){
        $scope.nomDonnee = "Niveau Scolaire"
        $rootScope.breadcrumb = "niveauScolaire";
        $scope.titre = "Ajouter un nouveau niveau scolaire";
    }
    $window.document.title = "ADMIN | "+$scope.nomDonnee+" - Ajouter";
    $rootScope.breadcrumbActive = "Ajouter";
    $rootScope.dataLoaded = true;
    var validateOnQuit = true;

    $scope.newDonnee = function($event){
        $event.preventDefault();
        $scope.donnees.push({});
    }

    $scope.removeDonnee = function($index){
        $scope.donnees.splice($index,1);
    }

    $scope.reset = function() {
        $scope.donnees = [{}];
    };

    $scope.save = function(donnees) {
        $scope.loading = true;
        $api.addDonnees(type, donnees)
            .then(function (result) {
                if(result.data.length == 1){
                    toastr.success('La donnée a été ajoutée avec succès');
                }
                else{
                    toastr.success('Les '+result.data.length+' données ont été ajoutées avec succès');
                }
                $scope.loading = false;
                validateOnQuit = false;
                url = "/donnee/voir/"+type;
                $location.path(url);
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };

    $scope.$on("$locationChangeStart", function(event) {
        var confirmed;
        if (!$scope.myForm.$pristine && validateOnQuit) {
            confirmed = $window.confirm("Il semblerait que vous ayez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
            if (event && !confirmed) { 
              event.preventDefault();
            }
        }
    });

}]);

myApp.controller('listeDonneeController',['$scope', '$log', '$window',  'Api', 'toastr', '$location','NgTableParams', '$rootScope', '$routeParams', '$ngConfirm', function($scope, $log, $window, api, toastr, $location, NgTableParams, $rootScope, $routeParams, $ngConfirm){
    var type = $routeParams.type;
    if(type == 1){
        $scope.nomDonnee = "Etablissement"
        $rootScope.breadcrumb = "etablissement";
    }
    else if(type == 2){
        $scope.nomDonnee = "Niveau Scolaire"
        $rootScope.breadcrumb = "niveauScolaire";
    }
    $window.document.title = "ADMIN | "+$scope.nomDonnee+"s - Liste";
    $rootScope.breadcrumbTitle = "Données";
    $rootScope.breadcrumbTitleUrlTemp = "/donnee/"+type;
    $rootScope.breadcrumbActive = "Liste";
    $scope.type = type;
    $rootScope.dataLoaded = false;
    $scope.donneesTable = new NgTableParams
    (
        {
            page: 1,
            count: 12,
            sorting: { nom: "asc" }
        },
        {
            getData: function(params) {
                params.url()['type'] = type;
                return api.getAllDonnees(params.url(), type).then(function(result) {
                    $rootScope.dataLoaded = true;
                    params.total(result.data.total);
                    return result.data.donnees;
                }).catch(function(result) {
                    if(result.status == 404) {
                        toastr.error(result.data.message);
                        url = "/";
                        validateOnQuit = false;
                        $location.path(url);
                    }
                });
            }
        }
    );

    $scope.remove = function($index, type, donnee){
        $ngConfirm({
            title: 'Attention!',
            content: 'Cette donnée ne pourra être récupérée',
            scope: $scope,
            theme: 'modern',
            buttons: {
                confirm: {
                    text: 'Confirmer',
                    btnClass: 'btn-blue',
                    action: function(scope, button){
                        console.log($scope.type);
                        api.removeDonnee(type, donnee)
                            .then(function (result) {
                                $scope.donneesTable.reload();
                                toastr.success('La donnée a été supprimée avec succès');
                                $scope.donnee = angular.copy($scope.master);
                                $scope.loading = false;
                            }, function (error) {
                                angular.forEach(error.data.errors.children, function(value, key) {
                                    angular.forEach(value.errors, function(value2, key2) {
                                        toastr.error('[ '+key+' ] '+value2);
                                    });
                                });
                                $scope.loading = false;
                                return false;
                            });
                    }
                },
                close: {
                    text: 'Fermer'
                },
            }
        });
    };

}]);


myApp.controller('editerDonneeController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', '$filter', 'moment', function($scope, $log, $window, $rootScope, $routeParams, api, toastr, $location, $filter, moment){
    var id = $routeParams.id;
    var type = $routeParams.type;
    $scope.type = type;
    if(type == 1){
        $scope.nomDonnee = "Etablissement"
        $rootScope.breadcrumb = "etablissement";
    }
    else if(type == 2){
        $scope.nomDonnee = "Niveau Scolaire"
        $rootScope.breadcrumb = "niveauScolaire";
    }
    $window.document.title = "ADMIN | "+$scope.nomDonnee+" - Editer";
    $scope.titre = "Editer "+$scope.nomDonnee;
    $rootScope.breadcrumbActive = "Editer";
    $rootScope.dataLoaded = false;
    var initialDonnee;
    var validateOnQuit = true;

    $scope.donnee = {};

    api.getDonnee(type, id).then(function(result) {
        $rootScope.dataLoaded = true;
        $scope.donnee = result.data;
        initialDonnee = angular.copy($scope.donnee);
        $scope.titre += " " + result.data.nom;
        $rootScope.breadcrumbActive += " "+$scope.donnee.nom;
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
            toastr.error(result.data.message);
            url = "/donnee/voir/"+type;
            validateOnQuit = false;
            $location.path(url);
        }
    });

    $scope.add = function(donnee) {
        $scope.loading = true;
        api.updateDonnee(type, donnee)
            .then(function (result) {
                toastr.success('La donnée a été modifiée avec succès');
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/donnee/voir/"+type);
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };


    $scope.reset = function() {
        $scope.donnee = angular.copy(initialDonnee);
    };

    $scope.$on("$locationChangeStart", function(event) {
      var confirmed;
      if (!angular.equals(initialDonnee, $scope.donnee) && validateOnQuit) {
        confirmed = $window.confirm("Attention ! Vous avez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
        if (event && !confirmed) { 
          event.preventDefault();
        }
      }
    });

}]);