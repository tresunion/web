myApp.controller('voirStatistiquesController',['$scope', '$log', '$window',  'Api', 'toastr', '$location','NgTableParams', '$rootScope', '$routeParams', '$ngConfirm', function($scope, $log, $window, api, toastr, $location, NgTableParams, $rootScope, $routeParams, $ngConfirm){
    $scope.nomDonnee = "Statistique";
    $rootScope.breadcrumb = "statistiques";
    $window.document.title = "ADMIN | " + $scope.nomDonnee + "s";
    $scope.titre = "Statistiques";
    $rootScope.breadcrumbTitle = "Statistiques";
    $rootScope.breadcrumbTitleUrlTemp = "/statistiques/voir";
    $rootScope.breadcrumbActive = "Liste";
    $rootScope.dataLoaded = false;

    api.getAllStatistiques().then(function(result) {
        $scope.evenement = result.data;

        $scope.elevesStats = result.data["eleves"];
        $scope.intervenantsStats = result.data["intervenants"];
        $scope.etablissementsStats = result.data["etablissements"];
        $scope.evenementsStats = result.data["evenements"];
        $scope.seancesStats = result.data["seances"];

        $rootScope.dataLoaded = true;

        drawElevesStats($scope.elevesStats);
        drawIntervenantsStats($scope.intervenantsStats);
        drawEtablissementsStats($scope.etablissementsStats);
        drawEvenementsStats($scope.evenementsStats);
        drawSeancesStats($scope.seancesStats);

        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
            toastr.error(result.data.message);
            url = "/statistiques/voir";
            $location.path(url);
        }
    });
}]);

function drawElevesStats(datas) {
    var ctxSexes = document.getElementById("chartElevesSexes");
    var ctxNaissance = document.getElementById("chartElevesNaissances");
    var ctxTelephones = document.getElementById("chartElevesTelephones");
    var ctxMails = document.getElementById("chartElevesMails");
    var ctxSeances = document.getElementById("chartElevesSeances");
    var ctxInscriptions = document.getElementById("chartElevesIncriptions");

    var chartElevesSexes = new Chart(ctxSexes, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbParSexes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParSexes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du sexe des élèves'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartElevesNaissances = new Chart(ctxNaissance, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbNaissancesParAnnees"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbNaissancesParAnnees"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de naissance des élèves (par années)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartElevesTelephones = new Chart(ctxTelephones, {
        type: 'doughnut',
        data: {
            labels: ["Numéros renseignés", "Numéros non renseignés"],
            datasets: [{
                label: 'Donnees',
                data: [datas["nbTelephones"], (datas["total"] - datas["nbTelephones"])],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du renseignement sur le numéro de téléphone des élèves'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartElevesMails = new Chart(ctxMails, {
        type: 'doughnut',
        data: {
            labels: ["Mails renseignés", "Mails non renseignés"],
            datasets: [{
                label: 'Donnees',
                data: [datas["nbMails"], (datas["total"] - datas["nbMails"])],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du renseignement sur l\'email des élèves'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartElevesSeances = new Chart(ctxSeances, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbSeancesParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbSeancesParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: "Répartition du nombre de séances des élèves (par paquets)"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartElevesIncriptions = new Chart(ctxInscriptions, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbInscriptionsParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbInscriptionsParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'inscriptions des élèves (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function drawIntervenantsStats(datas) {
    var ctxSexes = document.getElementById("chartIntervenantsSexes");
    var ctxNaissance = document.getElementById("chartIntervenantsNaissances");
    var ctxTelephones = document.getElementById("chartIntervenantsTelephones");
    var ctxMails = document.getElementById("chartIntervenantsMails");
    var ctxSeances = document.getElementById("chartIntervenantsSeances");
    var ctxInscriptions = document.getElementById("chartIntervenantsIncriptions");

    var chartIntervenantsSexes = new Chart(ctxSexes, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbParSexes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParSexes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du sexe des intervenants'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartIntervenantsNaissances = new Chart(ctxNaissance, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbNaissancesParAnnees"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbNaissancesParAnnees"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de naissance des intervenants (par années)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartIntervenantsTelephones = new Chart(ctxTelephones, {
        type: 'doughnut',
        data: {
            labels: ["Numéros renseignés", "Numéros non renseignés"],
            datasets: [{
                label: 'Donnees',
                data: [datas["nbTelephones"], (datas["total"] - datas["nbTelephones"])],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du renseignement sur le numéro de téléphone des intervenants'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartIntervenantsMails = new Chart(ctxMails, {
        type: 'doughnut',
        data: {
            labels: ["Mails renseignés", "Mails non renseignés"],
            datasets: [{
                label: 'Donnees',
                data: [datas["nbMails"], (datas["total"] - datas["nbMails"])],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du renseignement sur l\'email des intervenants'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartIntervenantsSeances = new Chart(ctxSeances, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbSeancesParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbSeancesParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: "Répartition du nombre de séances des intervenants (par paquets)"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartIntervenantsIncriptions = new Chart(ctxInscriptions, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbInscriptionsParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbInscriptionsParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'inscriptions des intervenants (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function drawEtablissementsStats(datas) {
    var ctxTypes = document.getElementById("chartEtablissementsTypes");

    var chartEtablissementsTypes = new Chart(ctxTypes, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbParTypes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParTypes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition des types d\'établissements'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function drawEvenementsStats(datas) {
    var ctxDatesD = document.getElementById("chartEvenementsDatesDebut");
    var ctxDatesF = document.getElementById("chartEvenementsDatesFin");
    var ctxDurees = document.getElementById("chartEvenementsDurees");
    var ctxStatuts = document.getElementById("chartEvenementsStatuts");
    var ctxListes = document.getElementById("chartEvenementsListes");
    var ctxEleves = document.getElementById("chartEvenementsEleves");

    var chartEvenementsDatesDebut = new Chart(ctxDatesD, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbDatesDebutParMois"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbDatesDebutParMois"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de début des évènements (par mois)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartEvenementsDatesFin = new Chart(ctxDatesF, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbDatesFinParMois"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbDatesFinParMois"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de fin des évènements (par mois)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartEvenementsDurees = new Chart(ctxDurees, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbParDurees"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParDurees"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la durée des évènements (en jours)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartEvenementsStatuts = new Chart(ctxStatuts, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbParStatuts"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParStatuts"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du statut des évènements'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartEvenementsListes = new Chart(ctxListes, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbEvenementsParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbEvenementsParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'évènements liés (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartEvenementsEleves = new Chart(ctxEleves, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbElevesParInscriptions"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbElevesParInscriptions"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'élèves inscrits aux évènements (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function drawSeancesStats(datas) {
    var ctxDatesD = document.getElementById("chartSeancesDatesDebut");
    var ctxDatesF = document.getElementById("chartSeancesDatesFin");
    var ctxDurees = document.getElementById("chartSeancesDurees");
    var ctxStatuts = document.getElementById("chartSeancesStatuts");
    var ctxEleves = document.getElementById("chartSeancesEleves");
    var ctxIntervenants = document.getElementById("chartSeancesIntervenants");

    var chartSeancesDatesDebut = new Chart(ctxDatesD, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbDatesDebutParMois"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbDatesDebutParMois"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de début des séances (par mois)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartSeancesDatesFin = new Chart(ctxDatesF, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbDatesFinParMois"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbDatesFinParMois"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la date de fin des séances (par mois)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartSeancesDurees = new Chart(ctxDurees, {
        type: 'bar',
        data: {
            labels: Object.keys(datas["nbParDurees"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParDurees"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition de la durée des séances (en jours)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartSeancesStatuts = new Chart(ctxStatuts, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbParStatuts"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbParStatuts"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du statut des séances'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartSeancesEleves = new Chart(ctxEleves, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbElevesParlistes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbElevesParlistes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'élèves aux séances (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var chartSeancesIntervenants = new Chart(ctxIntervenants, {
        type: 'doughnut',
        data: {
            labels: Object.keys(datas["nbIntervenantsParListes"]),
            datasets: [{
                label: 'Donnees',
                data: Object.values(datas["nbIntervenantsParListes"]),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top'
            },
            title: {
                display: true,
                text: 'Répartition du nombre d\'intervenants aux séances (par paquets)'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}