myApp.controller('editerIntervenantController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'toastr', 'Api', '$location', '$filter', function($scope, $log, $window, $rootScope, $routeParams, toastr, api, $location, $filter){
    $window.document.title = "ADMIN | Intervenants - Editer";
    $rootScope.breadcrumb = "intervenants";
    $scope.titre = "Editer intervenant ";
    $rootScope.breadcrumbActive = "Editer";
    var id = $routeParams.id;
    $rootScope.dataLoaded = false;
    var initialIntervenant;
    var validateOnQuit = true;

    $scope.intervenant = {};

    api.getIntervenant(id).then(function(result) {
        $scope.intervenant = result.data;
        $rootScope.dataLoaded = true;
        if($scope.intervenant.dateNaissance){ 
            $scope.dateNaissanceString = $filter('date')($scope.intervenant.dateNaissance, 'dd/MM/yyyy');
            $scope.intervenant.dateNaissance = $filter('date')($scope.intervenant.dateNaissance, 'dd/MM/yyyy');
        }
        initialIntervenant = angular.copy($scope.intervenant);
        $scope.titre += result.data.nom + " "+result.data.prenom;
        $rootScope.breadcrumbActive += " "+result.data.nom+" "+$scope.intervenant.prenom;
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
			toastr.error(result.data.message);
            url = "/intervenant/liste";
            validateOnQuit = false;
			$location.path(url);
        }
    });

    $scope.add = function(intervenant) {
        $scope.loading = true;
        api.updateIntervenant(intervenant)
            .then(function (result) {
                toastr.success('L\'intervenant ' + intervenant.nom + ' ' + intervenant.prenom + ' a été modifié avec succès');
                $scope.intervenant = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/intervenant/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };


    $scope.reset = function(intervenantid) {
        $scope.intervenant = angular.copy(initialIntervenant);
    };

    $scope.$on("$locationChangeStart", function(event) {
      var confirmed;
      if (!angular.equals(initialIntervenant, $scope.intervenant) && validateOnQuit) {
        confirmed = $window.confirm("Attention ! Il semblerait que vous ayez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
        if (event && !confirmed) { 
          event.preventDefault();
        }
      }
    });

}]);

myApp.controller('listeIntervenantController',['$scope', '$log', '$window',  'Api', 'toastr', '$location','NgTableParams', '$rootScope', '$ngConfirm', function($scope, $log, $window, api, toastr, $location, NgTableParams, $rootScope, $ngConfirm){
    $window.document.title = "ADMIN | Intervenants - Liste";
    $rootScope.breadcrumbTitle = "Intervenants";
    $rootScope.breadcrumb = "intervenants";
    $rootScope.breadcrumbActive = "Liste";
    $rootScope.dataLoaded = false;

    $scope.intervenantsTable = new NgTableParams
    (
        {
            page: 1,
            count: 12,
            sorting: { nom: "asc" }
        },
        {
            getData: function(params) {
                return api.getAllIntervenants(params.url()).then(function(result) {
                    $rootScope.dataLoaded = true;
                    params.total(result.data.total);
                    return result.data.intervenants;
                });
            }
        }
    );

    $scope.remove = function($index, intervenant){
        $ngConfirm({
            title: 'Attention!',
            content: 'Cet intervenant ne pourra être récupéré',
            scope: $scope,
            theme: 'modern',
            buttons: {
                confirm: {
                    text: 'Confirmer',
                    btnClass: 'btn-blue',
                    action: function(scope, button){
                        api.removeIntervenant(intervenant)
                            .then(function (result) {
                                $scope.intervenantsTable.reload();
                                toastr.success('L\'intervenant ' + intervenant.nom + ' ' + intervenant.prenom + ' a été supprimé avec succès');
                                $scope.intervenant = angular.copy($scope.master);
                                $scope.loading = false;
                            }, function (error) {
                                angular.forEach(error.data.errors.children, function(value, key) {
                                    angular.forEach(value.errors, function(value2, key2) {
                                        toastr.error('[ '+key+' ] '+value2);
                                    });
                                });
                                $scope.loading = false;
                                return false;
                            });
                    }
                },
                close: {
                    text: 'Fermer'
                },
            }
        });
    };
}]);

myApp.controller('ajouterIntervenantController', ['$scope', '$log', '$window', 'Api', 'toastr', '$location', '$rootScope', '$ngConfirm', '$filter', function($scope, $log, $window, $api, toastr, $location, $rootScope, $ngConfirm, $filter) {
    $scope.master = {};
    $rootScope.breadcrumbTitle = "Intervenants";
    $window.document.title = "ADMIN | Intervenants - Ajouter";
    $rootScope.breadcrumb = "intervenants";
    $rootScope.breadcrumbActive = "Ajouter";
    $scope.titre = "Ajouter un nouvel intervenant";
    $rootScope.dataLoaded = true;
    var validateOnQuit = true;

    $scope.intervenant = {};

    $scope.add = function(intervenant) {
        $scope.loading = true;
        $api.postIntervenant(intervenant)
            .then(function (result) {
                toastr.success('L\'intervenant ' + intervenant.nom + ' ' + intervenant.prenom + ' a été ajouté avec succès');
                $scope.intervenant = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/intervenant/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };

    $scope.reset = function(intervenantId) {
        $scope.intervenant = angular.copy($scope.master);
    };

    $scope.$on("$locationChangeStart", function(event) {
        var confirmed;
        if (!$scope.myForm.$pristine && validateOnQuit) {
            confirmed = $window.confirm("Il semblerait que vous ayez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
            if (event && !confirmed) { 
              event.preventDefault();
            }
        }
    });

}]);