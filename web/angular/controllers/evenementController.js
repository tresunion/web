myApp.controller('editerEvenementController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', '$filter', 'moment', function($scope, $log, $window, $rootScope, $routeParams, api, toastr, $location, $filter, moment){
    $window.document.title = "ADMIN | Evenements - Editer";
    $rootScope.breadcrumb = "evenement";
    $scope.titre = "Editer évènement ";
    $rootScope.breadcrumbActive = "Editer";
    $rootScope.dataLoaded = false;
    var id = $routeParams.id;
    var initialEvenement;
    var validateOnQuit = true;

    $scope.evenement = {};

    api.getEvenement(id).then(function(result) {
        $rootScope.dataLoaded = true;
        $scope.evenement = result.data;
        $scope.evenement.dateDebut = new Date($scope.evenement.date_debut);
        $scope.evenement.dateFin = new Date($scope.evenement.date_fin);
        initialEvenement = angular.copy($scope.evenement);
        $scope.titre += result.data.libelle;
        $rootScope.breadcrumbActive += " "+result.data.libelle;
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
			toastr.error(result.data.message);
            url = "/evenement/liste";
            validateOnQuit = false;
			$location.path(url);
        }
    });

    $scope.add = function(evenement) {
        console.log(evenement);
        $scope.loading = true;
        api.updateEvenement(evenement)
            .then(function (result) {
                toastr.success('L\'évènement '+ evenement.libelle+' a été modifié avec succès');
                $scope.evenement = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/evenement/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };


    $scope.reset = function(evenementid) {
        $scope.evenement = angular.copy(initialEvenement);
    };

    $scope.$on("$locationChangeStart", function(event) {
      var confirmed;
      if (!angular.equals(initialEvenement, $scope.evenement) && validateOnQuit) {
        confirmed = $window.confirm("Attention ! Vous avez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
        if (event && !confirmed) { 
          event.preventDefault();
        }
      }
    });

}]);

myApp.controller('listeEvenementController',['$scope', '$log', '$window',  'Api', 'toastr', '$location','NgTableParams', '$rootScope', '$ngConfirm', 'Api','moment', function($scope, $log, $window, api, toastr, $location, NgTableParams, $rootScope, $ngConfirm, $api, moment){
    $window.document.title = "ADMIN | Evènements - Liste";
    $rootScope.breadcrumbTitle = "Evènements";
    $rootScope.breadcrumb = "evenement";
    $rootScope.breadcrumbActive = "Liste";
    $rootScope.dataLoaded = false;
    $scope.dateNow = new Date();

    $scope.evenementsTable = new NgTableParams
    (
        {
            page: 1,
            count: 20,
            sorting: { dateDebut: "desc" }
        },
        {
            getData: function(params) {
                return api.getAllEvenements(params.url()).then(function(result) {
                    $rootScope.dataLoaded = true;
                    params.total(result.data.total);
                    return result.data.evenements;
                });
            }
        }
    );

    $scope.getStatutEvenement = function(evenement) {
        if(moment(evenement.date_debut).isAfter(moment())){
            return 1;
        }

        if(moment(evenement.date_debut).isBefore(moment()) && moment(evenement.date_fin).isAfter(moment())){
            return 2;
        }

        if(moment(evenement.date_fin).isBefore(moment())){
            return 3;
        }
    };

    $scope.remove = function($index, evenement){
        $ngConfirm({
            title: 'Attention!',
            content: 'Cet évènement ne pourra être récupéré',
            scope: $scope,
            theme: 'modern',
            buttons: {
                confirm: {
                    text: 'Confirmer',
                    btnClass: 'btn-blue',
                    action: function(scope, button){
                        $api.removeEvenement(evenement)
                            .then(function (result) {
                                $scope.evenementsTable.reload();
                                toastr.success('L\'évènement '+ evenement.libelle+' a été supprimé avec succès');
                                $scope.evenement = angular.copy($scope.master);
                                $scope.loading = false;
                            }, function (error) {
                                if(error.data.message){
                                    toastr.error(error.data.message);
                                }
                                else{
                                    angular.forEach(error.data.errors.children, function(value, key) {
                                        angular.forEach(value.errors, function(value2, key2) {
                                            toastr.error('[ '+key+' ] '+value2);
                                        });
                                    });
                                }
                                $scope.loading = false;
                                return false;
                            });
                    }
                },
                close: {
                    text: 'Fermer'
                },
            }
        });
    };
}]);

myApp.controller('ajouterEvenementController', ['$scope', '$log', '$window', 'Api', 'toastr', '$location', '$rootScope', '$filter', function($scope, $log, $window, $api, toastr, $location, $rootScope, $filter) {
    $scope.master = {};
    $window.document.title = "ADMIN | Evenements - Ajouter";
    $rootScope.breadcrumbTitle = "Evenements";
    $rootScope.breadcrumb = "evenement";
    $rootScope.breadcrumbActive = "Ajouter";
    $scope.titre = "Ajouter un nouvel evenement";
    $rootScope.dataLoaded = true;
    var validateOnQuit = true;

    $scope.evenement = {};

    $scope.add = function(evenement) {
        $scope.loading = true;
        $api.postEvenement(evenement)
            .then(function (result) {
                toastr.success('L\'evenement '+ evenement.libelle+' a été ajouté avec succès');
                $scope.evenement = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/evenement/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };

    $scope.reset = function(evenementid) {
        $scope.evenement = angular.copy($scope.master);
    };

    $scope.$on("$locationChangeStart", function(event) {
        var confirmed;
        if (!$scope.myForm.$pristine && validateOnQuit) {
            confirmed = $window.confirm("Il semblerait que vous ayez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
            if (event && !confirmed) { 
              event.preventDefault();
            }
        }
    });

}]);

myApp.controller('statsEvenementController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', '$filter', 'moment', function($scope, $log, $window, $rootScope, $routeParams, api, toastr, $location, $filter, moment){
    $window.document.title = "ADMIN | Evènements - statistiques";
    $rootScope.breadcrumb = "evenement";
    $scope.titre = "";
    $rootScope.breadcrumbActive = "Statistiques";
    $rootScope.dataLoaded2 = false;
    $rootScope.dataLoaded = true;
    var id = $routeParams.id;

    $scope.evenement = {};

    $scope.options1 = {
        chart: {
            type: 'pieChart',
            x: function(d){return d.key;},
            y: function(d){return d.y;},
            showLabels: true,
            duration: 500,
            labelThreshold: 0.01,
            labelSunbeamLayout: true,
            height: 200,
            legend: {
                margin: {
                    top: 5,
                    right: 150,
                    bottom: 5,
                    left: 0
                }
            }
        }
    };

    $scope.options2 = {
        chart: {
            type: 'discreteBarChart',
            height: 200,
            margin : {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function(d){return d.label;},
            y: function(d){return d.value;},
            showValues: true,
            valueFormat: function(d){
                return d3.format(',.4f')(d);
            },
            duration: 500,
            xAxis: {
                axisLabel: 'Age'
            },
            yAxis: {
                axisLabel: 'Quantité',
                axisLabelDistance: -10
            }
        }
    };

    api.getEvenement(id).then(function(result) {
        $scope.evenement = result.data;
        $scope.titre += result.data.libelle;
        $rootScope.breadcrumbActive += " "+result.data.libelle;
        api.getRepartitionHommeFemme(id).then(function(result){
            $scope.data1 = result.data.data;
            api.getRepartitionAge(id).then(function(result){
                $scope.data2 = result.data.data;
                api.getRepartitionEtablissement(id).then(function(result){
                    $scope.data3 = result.data.data ;
                    api.getRepartitionNiveauScolaire(id).then(function(result){
                        $scope.data4 = result.data.data ;
                        $rootScope.dataLoaded2 = true;
                        return result.data;
                    });
                    return result.data;
                });
                return result.data;
            });
            return result.data;
        });
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
            toastr.error(result.data.message);
            url = "/evenement/liste";
            $location.path(url);
        }
    });



}]);


myApp.controller('detailsEvenementController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', '$filter', 'moment', function($scope, $log, $window, $rootScope, $routeParams, api, toastr, $location, $filter, moment){
    $window.document.title = "ADMIN | Evènements - Détails";
    $rootScope.breadcrumb = "evenement";
    $scope.titre = "";
    $rootScope.breadcrumbActive = "Détails";
    $rootScope.dataLoaded = true;
    var id = $routeParams.id;

    $scope.evenement = {};

    api.getEvenement(id).then(function(result) {
        $scope.evenement = result.data;
        $scope.titre += result.data.libelle;
        $rootScope.breadcrumbActive += " "+result.data.libelle;
        api.getRepartitionHommeFemme(id).then(function(result){
            $scope.data1 = result.data.data;
            api.getRepartitionAge(id).then(function(result){
                $scope.data2 = result.data.data;
                api.getRepartitionEtablissement(id).then(function(result){
                    $scope.data3 = result.data.data ;
                    api.getRepartitionNiveauScolaire(id).then(function(result){
                        $scope.data4 = result.data.data ;
                        $rootScope.dataLoaded2 = true;
                        return result.data;
                    });
                    return result.data;
                });
                return result.data;
            });
            return result.data;
        });
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
            toastr.error(result.data.message);
            url = "/evenement/liste";
            $location.path(url);
        }
    });



}]);