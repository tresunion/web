myApp.controller('editerEleveController',['$scope', '$log', '$window', '$rootScope', '$routeParams', 'Api', 'toastr', '$location', '$filter', 'moment', function($scope, $log, $window, $rootScope, $routeParams, api, toastr, $location, $filter, moment){
    $window.document.title = "ADMIN | Eleves - Editer";
    $rootScope.breadcrumb = "eleves";
    $scope.titre = "Editer élève ";
    $rootScope.breadcrumbActive = "Editer";
    $rootScope.dataLoaded = false;
    var id = $routeParams.id;
    var initialEleve;
    var validateOnQuit = true;

    $scope.eleve = {};

    api.getEleve(id).then(function(result) {
        $rootScope.dataLoaded = true;
        $scope.eleve = result.data;
        $scope.eleve.dateNaissance = new Date($scope.eleve.date_naissance);
        initialEleve = angular.copy($scope.eleve);
        $scope.titre += result.data.nom + " "+result.data.prenom;
        $rootScope.breadcrumbActive += " "+result.data.nom+" "+$scope.eleve.prenom;
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
			toastr.error(result.data.message);
            url = "/eleve/liste";
            validateOnQuit = false;
			$location.path(url);
        }
    });

    $scope.add = function(eleve) {
        console.log(eleve);
        $scope.loading = true;
        api.updateEleve(eleve)
            .then(function (result) {
                toastr.success('L\'élève '+ eleve.nom+' '+eleve.prenom+' a été modifié avec succès');
                $scope.eleve = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/eleve/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };


    $scope.reset = function(eleveid) {
        $scope.eleve = angular.copy(initialEleve);
    };

    $scope.$on("$locationChangeStart", function(event) {
      var confirmed;
      if (!angular.equals(initialEleve, $scope.eleve) && validateOnQuit) {
        confirmed = $window.confirm("Attention ! Vous avez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
        if (event && !confirmed) { 
          event.preventDefault();
        }
      }
    });

}]);

myApp.controller('listeEleveController',['$scope', '$log', '$window',  'Api', 'toastr', '$location','NgTableParams', '$rootScope', '$ngConfirm', 'Api', function($scope, $log, $window, api, toastr, $location, NgTableParams, $rootScope, $ngConfirm, $api){
    $window.document.title = "ADMIN | Eleves - Liste";
    $rootScope.breadcrumbTitle = "Eleves";
    $rootScope.breadcrumb = "eleves";
    $rootScope.breadcrumbActive = "Liste";
    $rootScope.dataLoaded = false;

    $scope.elevesTable = new NgTableParams
    (
        {
            page: 1,
            count: 12,
            sorting: { nom: "asc" }
        },
        {
            getData: function(params) {
                return api.getAllEleves(params.url()).then(function(result) {
                    $rootScope.dataLoaded = true;
                    params.total(result.data.total);
                    return result.data.eleves;
                });
            }
        }
    );

    $scope.remove = function($index, eleve){
        $ngConfirm({
            title: 'Attention!',
            content: 'Cet élève ne pourra être récupéré',
            scope: $scope,
            theme: 'modern',
            buttons: {
                confirm: {
                    text: 'Confirmer',
                    btnClass: 'btn-blue',
                    action: function(scope, button){
                        $api.removeEleve(eleve)
                            .then(function (result) {
                                $scope.elevesTable.reload();
                                toastr.success('L\'élève '+ eleve.nom+' '+eleve.prenom+' a été supprimé avec succès');
                                $scope.eleve = angular.copy($scope.master);
                                $scope.loading = false;
                            }, function (error) {
                                angular.forEach(error.data.errors.children, function(value, key) {
                                    angular.forEach(value.errors, function(value2, key2) {
                                        toastr.error('[ '+key+' ] '+value2);
                                    });
                                });
                                $scope.loading = false;
                                return false;
                            });
                    }
                },
                close: {
                    text: 'Fermer'
                },
            }
        });
    };
}]);

myApp.controller('ajouterEleveController', ['$scope', '$log', '$window', 'Api', 'toastr', '$location', '$rootScope', '$filter', function($scope, $log, $window, $api, toastr, $location, $rootScope, $filter) {
    $scope.master = {};
    $window.document.title = "ADMIN | Eleves - Ajouter";
    $rootScope.breadcrumbTitle = "Eleves";
    $rootScope.breadcrumb = "eleves";
    $rootScope.breadcrumbActive = "Ajouter";
    $scope.titre = "Ajouter un nouvel élève";
    $rootScope.dataLoaded = true;
    var validateOnQuit = true;

    $scope.eleve = {};

    $scope.add = function(eleve) {
        $scope.loading = true;
        $api.postEleve(eleve)
            .then(function (result) {
                toastr.success('L\'élève '+ eleve.nom+' '+eleve.prenom+' a été ajouté avec succès');
                $scope.eleve = angular.copy($scope.master);
                $scope.loading = false;
                validateOnQuit = false;
                $location.path("/eleve/liste");
            }, function (error) {
                angular.forEach(error.data.errors.children, function(value, key) {
                    angular.forEach(value.errors, function(value2, key2) {
                        toastr.error('[ '+key+' ] '+value2);
                    });
                });
                $scope.loading = false;
            });
    };

    $scope.reset = function(eleveid) {
        $scope.eleve = angular.copy($scope.master);
    };

    $scope.$on("$locationChangeStart", function(event) {
        var confirmed;
        if (!$scope.myForm.$pristine && validateOnQuit) {
            confirmed = $window.confirm("Il semblerait que vous ayez des changements non sauvegardés. Voulez-vous vraiment quitter cette page ?");
            if (event && !confirmed) { 
              event.preventDefault();
            }
        }
    });

}]);