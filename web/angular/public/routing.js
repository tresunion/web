myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    
    .when("/inscription/:id", {
        templateUrl: "/angular/public/views/inscription.html",
        controller: 'inscriptionController'
    })
    
    .when("/", {
        templateUrl: "/angular/public/views/home.html",
        controller: 'homeController'
    })
    
    // DEFAULT
    .otherwise({ redirectTo: "/" });
}]);