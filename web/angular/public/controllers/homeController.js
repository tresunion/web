myApp.controller('homeController', ['$scope', '$log', '$window', 'Api', 'toastr', '$location', '$rootScope', '$filter', function($scope, $log, $window, $api, toastr, $location, $rootScope, $filter) {
    $scope.master = {};
    $window.document.title = "Très d'Union | Public";
    $rootScope.dataLoaded = false;
    var validateOnQuit = true;

    $scope.getStatutEvenement = function(evenement) {
        if(moment(evenement.date_debut).isAfter(moment())){
            return 1;
        }

        if(moment(evenement.date_debut).isBefore(moment()) && moment(evenement.date_fin).isAfter(moment())){
            return 2;
        }

        if(moment(evenement.date_fin).isBefore(moment())){
            return 3;
        }
    };

    $api.getAllEvenements().then(function(result) {
        evenements = result.data.evenements;
        $scope.evenements = evenements;
        $rootScope.dataLoaded = true;
        return result.data;
    });

}]);