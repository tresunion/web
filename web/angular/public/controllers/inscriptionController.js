myApp.controller('inscriptionController', ['$scope', '$log', '$window', 'Api', 'toastr', '$location', '$rootScope', '$filter', '$routeParams', function($scope, $log, $window, $api, toastr, $location, $rootScope, $filter, $routeParams) {
    $rootScope.dataLoaded = false;
    var validateOnQuit = true;

    $scope.eleve = {};
    $scope.inscription = {};
    $scope.etablissements = {};
    $scope.niveauxScolaires = {};
    $scope.evenement = {};

    var id = $routeParams.id;

    $scope.getStatutEvenement = function(evenement) {
        if(moment(evenement.date_debut).isAfter(moment())){
            return 1;
        }

        if(moment(evenement.date_debut).isBefore(moment()) && moment(evenement.date_fin).isAfter(moment())){
            return 2;
        }

        if(moment(evenement.date_fin).isBefore(moment())){
            return 3;
        }
    };

    $api.getEvenement(id).then(function(result) {
        $scope.evenement = result.data;
        $scope.inscription.evenement = result.data;
        $window.document.title = "Pré-Inscription | "+$scope.evenement.libelle;
        $scope.titre = "Inscription pour "+$scope.evenement.libelle+" du "+$filter('date')($scope.evenement.date_debut, 'dd/MM/yyyy')+" au "+$filter('date')($scope.evenement.date_fin, 'dd/MM/yyyy');
        $api.getEtablissements().then(function(result) {
            etablissements = result.data.etablissements;
            $scope.etablissements = etablissements;
            $api.getNiveauxScolaires().then(function(result) {
                niveauxScolaires = result.data.niveauxScolaires;
                $scope.niveauxScolaires = niveauxScolaires;
                $rootScope.dataLoaded = true;
                return result.data;
            });
            return result.data;
        });
        return result.data;
    }).catch(function(result) {
        if(result.status == 404) {
            toastr.error(result.data.message);
            url = ".";
            validateOnQuit = false;
            $location.path(url);
        }
    });

    $scope.add = function(eleve) {
        $scope.loading = true;
        inscriptionCopy = angular.copy($scope.inscription);
        if($scope.inscription.etablissement){
            $scope.inscription.etablissement = $scope.inscription.etablissement.id;
        }
        if($scope.inscription.niveauScolaire){
            $scope.inscription.niveauScolaire = $scope.inscription.niveauScolaire.id;
        }
        $scope.inscription.evenement = $scope.inscription.evenement.id;
        eleve.inscription = $scope.inscription;
        $scope.inscription = inscriptionCopy;
        $api.postEleve(eleve)
            .then(function (result) {
                toastr.success('Votre pré-inscription a bien été prise en compte.');
                $scope.loading = false;
                validateOnQuit = false;
                $location.path(".");
            }, function (error) {
                if(error.data.errors){
                    angular.forEach(error.data.errors.children, function(value, key) {
                        angular.forEach(value.errors, function(value2, key2) {
                            toastr.error('[ '+key+' ] '+value2);
                        });
                    });
                }else if(error.data.message){
                    toastr.error(error.data.message);
                }
                else{
                    toastr.error("Il y a eu un problème");
                }
                $scope.loading = false;
            });
    };

    $scope.reset = function(eleveid) {
        $scope.eleve = {};
        $scope.inscription = {};
    };

    $scope.$on("$locationChangeStart", function(event) {
        var confirmed;
        if (!$scope.myForm.$pristine && validateOnQuit) {
            confirmed = $window.confirm("En quittant cette page vous perdrez toutes les informations saisies. Voulez-vous vraiment quitter cette page ?");
            if (event && !confirmed) { 
              event.preventDefault();
            }
        }
    });

}]);