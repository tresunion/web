myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        // ELEVES
        .when("/eleve/ajouter", {
            templateUrl: "/angular/views/eleves/ajouter.html",
            controller: 'ajouterEleveController'
        })
        .when("/eleve/liste", {
            templateUrl: "/angular/views/eleves/liste.html",
            controller: 'listeEleveController'
        })
        .when("/eleve/editer/:id", {
            templateUrl: "/angular/views/eleves/ajouter.html",
            controller: 'editerEleveController'
        })

        //INTERVENANTS
        .when("/intervenant/ajouter", {
            templateUrl: "/angular/views/intervenants/ajouter.html",
            controller: 'ajouterIntervenantController'
        })
        .when("/intervenant/liste", {
            templateUrl: "/angular/views/intervenants/liste.html",
            controller: 'listeIntervenantController'
        })
        .when("/intervenant/editer/:id", {
            templateUrl: "/angular/views/intervenants/ajouter.html",
            controller: 'editerIntervenantController'
        })

        //EVENEMENTS
        .when("/evenement/ajouter", {
            templateUrl: "/angular/views/evenements/ajouter.html",
            controller: 'ajouterEvenementController'
        })
        .when("/evenement/liste", {
            templateUrl: "/angular/views/evenements/liste.html",
            controller: 'listeEvenementController'
        })
        .when("/evenement/editer/:id", {
            templateUrl: "/angular/views/evenements/ajouter.html",
            controller: 'editerEvenementController'
        })
        .when("/evenement/stats/:id", {
            templateUrl: "/angular/views/evenements/stats.html",
            controller: 'statsEvenementController'
        })
        .when("/evenement/details/:id", {
            templateUrl: "/angular/views/evenements/details.html",
            controller: 'detailsEvenementController'
        })

        // DONNEES
        .when("/donnee/voir/:type", {
            templateUrl: "/angular/views/donnees/liste.html",
            controller: 'listeDonneeController'
        })
        .when("/donnee/ajouter/:type", {
            templateUrl: "/angular/views/donnees/ajouter.html",
            controller: 'ajouterDonneeController'
        })
        .when("/donnee/editer/:type/:id", {
            templateUrl: "/angular/views/donnees/editer.html",
            controller: 'editerDonneeController'
        })

        // PLANNING
        .when("/planning", {
            templateUrl: "/angular/views/planning/planningGeneral.html",
            controller: 'planningController'
        })

        // STATISTIQUES
        .when("/statistiques/voir", {
            templateUrl: "/angular/views/statistiques/liste.html",
            controller: 'voirStatistiquesController'
        })

        // HOME
        .when("/", {
            templateUrl: "/angular/views/home.html"
        })

        // DEFAULT
        .otherwise({ redirectTo: "/" });
}]);